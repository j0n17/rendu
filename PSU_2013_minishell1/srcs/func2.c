/*
** func2.c for func2.c in /home/maire_j/rendu/PSU_2017_minishell1
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov 10 21:27:39 2013 Maire Jonathan
** Last update Sun Nov 10 22:20:21 2013 Maire Jonathan
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "../includes/hminishell.h"
#include "../libmy/my.h"

void    change_dir(char *buff, char **env)
{
  int   res;
  char  *user_path;
  char  **bufftab;

  bufftab = my_str_to_wordtab(buff);
  if (!bufftab[1])
    {
      user_path = my_rabb(user_tab("HOME", env), 5);
      res = chdir(user_path);
      if (res == -1)
	{
	  my_putstr(my_str_to_wordtab(buff)[0]);
	  my_putstr(": No such file or directory\n");
	}
      free(user_path);
    }
  else
    {
      res = chdir(bufftab[1]);
      if (res == -1)
	{
	  my_putstr(my_str_to_wordtab(buff)[0]);
	  my_putstr(": No such file or directory\n");
	}
    }
}

void	unset_env(char *str)
{
  if (unsetenv(str) == 0)
    my_putstr("Succes !\n");
  else
    perror("unset");
}
