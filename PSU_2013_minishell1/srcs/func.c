/*
** func.c for func.c in /home/maire_j/rendu/PSU_2017_minishell1
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Nov  4 12:54:44 2013 Maire Jonathan
** Last update Sun Nov 10 21:00:15 2013 Maire Jonathan
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdlib.h>
#include "../includes/hminishell.h"
#include "../libmy/my.h"

char	*my_catstr(char *src, char *dest)
{
  char	*res;
  int	i;
  int	k;

  i = 0;
  k = 0;
  if ((res = malloc(my_strlen(src) + my_strlen(dest) + 1)) == 0)
    return (0);
  while (src[i])
    {
      res[k] = src[i];
      k = k + 1;
      i = i + 1;
    }
  i = 0;
  while (dest[i])
    {
      res[k] = dest[i];
      i = i + 1;
      k = k + 1;
    }
  res[k] = '\0';
  return (res);
}

void    empty_buff(char *buff, int size)
{
  int   i;

  i = 0;
  while (i < size)
    {
      buff[i] = '\0';
      i = i + 1;
    }
}

char	*my_rabb(char *str, int size)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i < size)
    i = i + 1;
  while (str[i])
    {
      str[j] = str[i];
      i = i + 1;
      j = j + 1;
    }
  str[j] = '\0';
  return (str);
}
