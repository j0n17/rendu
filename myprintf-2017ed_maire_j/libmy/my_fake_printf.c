/*
** my_fake_printf.c for fake_printf in /home/maire_j/.libmy/my
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 31 15:49:26 2013 Maire Jonathan
** Last update Thu Oct 31 15:50:30 2013 Maire Jonathan
*/

void	my_printf(char *str, char *str2)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (str[i])
    {
      if (str[i] == '%' && str[i + 1] == '%')
        {
          while (str2[j])
            {
              my_putchar(str2[j]);
              j = j + 1;
            }
          i = i + 2;
        }
      my_putchar(str[i]);
      i = i + 1;
    }
}
