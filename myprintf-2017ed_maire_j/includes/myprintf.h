/*
** myprintf.h for myprintf.g in /home/maire_j/rendu/my_printf
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Nov 11 09:03:57 2013 Maire Jonathan
** Last update Mon Nov 11 09:40:11 2013 Maire Jonathan
*/

#ifndef MYPRINTF_H_
# define MYPRINTF_H_

int	count_args(char *);
int	my_printf(char *);

#endif /* MYPRINTF_H_ */
