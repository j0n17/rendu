/*
** main.c for ff in /home/maire_j/.Cfiles
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Nov  5 11:42:06 2013 Maire Jonathan
** Last update Wed Nov 13 13:07:11 2013 
*/

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include "../includes/myprintf.h"
#include "../libmy/my.h"

int    search_flag(char c)
{
  char    *ref;
  int    i;

  ref = "dicouxXbsS%plL";
  i = 0;
  while (ref[i])
    {
      if (c == ref[i])
	return (i);
      i++;
    }
  return (-1);
}

void    init_funct_printf(int (**function)(va_list ap))
{
  function[0] = &write_nbr;
  function[1] = &write_nbr;
  function[2] = &write_char;
  function[8] = &write_str;
  function[9] = &write_if_printable;
  function[10] = &write_modulo;
}

int        my_printf2(const char *format, ...)
{
  va_list    ap;
  int        (*function[12])(va_list ap);
  int        count;
  int        i;

  init_funct_printf(function);
  count = 0;
  va_start(ap, format);
  i = 0;
  while (format[i])
    {
      if (format[i] == '%' && search_flag(format[i + 1]) != -1)
	{
	  count = function[search_flag(format[i + 1])](ap) - 2 + count;
	  i++;
	}
      else
	my_putchar(format[i]);
      i++;
    }
  va_end(ap);
  count = count + i;
  return (count);
}

int	main()
{
  my_printf2("Je suis ta %s, %d, %c", "coucou", 8, 'c');
  return (0);
}

int     write_nbr(va_list ap)
{
  int   nb;
  int   count;

  nb = va_arg(ap, int);
  count = 0;
  if (nb < 0)
    count++;
  my_putnbr(nb);
  count = my_nbrlen(nb) + count;
  return (count);
}

int     write_char(va_list ap)
{
  char   c;

  c = va_arg(ap, int);
  my_putchar(c);
  return (1);
}

int    write_str(va_list ap)
{
  char    *str;
  int    count;

  str = va_arg(ap, char *);
  my_putstr(str);
  count = my_strlen(str);
  return (count);
}

int     write_if_printable(va_list ap)
{
  char  *str;
  int   count;

  str = va_arg(ap, char *);
  count = my_strlen(str);
  return (count);
}

int    write_modulo(va_list ap)
{
  (void)(ap);
  my_putchar('%');
  return (1);
}

int	my_nbrlen(int nb)
{
  int	i;
  int	res;

  i = 0;
  res = nb;
  while (res >= 10)
}
