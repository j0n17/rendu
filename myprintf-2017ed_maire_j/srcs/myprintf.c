/*
** myprintf.c for printf in /home/maire_j/rendu/my_printf/srcs
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Nov 11 08:55:14 2013 Maire Jonathan
** Last update Wed Nov 13 13:02:00 2013 
*/

#include <stdarg.h>
#include "../libmy/my.h"

int	count_args(char *st)
{
  int	i;
  int	j;
  int	nb;

  i = 0;
  j = 0;
  nb = 0;
  while (st[i] && st[j])
    {
      if (st[i] == '%' && st[i + 1] != '%')
	{
	  my_putchar('a');
	}
      j = j + 1;
      i = i + 1;
    }
  return (nb);
}

int	check_flag(char c)
{
  if (str[pos] == '%' && (str[pos + 1] == '%' || str[pos - 1] == '%'))
    return (0);
  else
    return (1);
}

int	my_printf(char *format, ...)
{
  int	size;
  int	i;

  i = 0;
  size = count_args(format);
  if (size == 0)
    my_putstr(format);
  else
    {
      if (check_flag(format, i) == 0)
	my_putchar(format[i]);
      else
	tab_func(format[i]);
    }
  return (0);
}
