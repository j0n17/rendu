/*
** my_ls.h for  in /home/maire_j/rendu/myls-2013-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 28 11:50:32 2013 Maire Jonathan
** Last update Tue Oct 29 10:54:46 2013 Maire Jonathan
*/

#ifndef MYLS_H_
# define MYLS_H_

struct dirent *pdir;

int	count_files(int, char **);
int	count_command(int, char **);
char	**get_files_name(int, char **);
char	**get_commands(int, char **);
void	process_args(int, char **);
void	proc_args(int, char **);

#endif /* MYLS_H_ */
