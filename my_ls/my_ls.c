/*
** my_ls.c for my_ls in /home/maire_j/rendu/myls-2013-maire_j
**
-** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
**
** Started on  Mon Oct 28 11:54:07 2013 Maire Jonathan
** Last update Thu Oct 31 09:05:02 2013 Maire Jonathan
*/

#include "myls.h"
#include "libmy/my.h"
#include <dirent.h>
#include <stdlib.h>

int        no_arg()
{
  DIR        *dirp;

  dirp = opendir(".");
  if (dirp == 0)
    {
      my_putstr("Error !");
      return (1);
    }
  while ((pdir = readdir(dirp)))
    {
      if (my_strncmp(pdir->d_name, ".", 1))
	{
	  if (pdir->d_type == 8)
	    {
	      my_putstr("\e[1;34m");
	      my_putstr(pdir->d_name);
	      my_putstr("\e[0m");
	    }
	  else
	    {
	      my_putchar('a');
	      my_putstr(pdir->d_name);
	    }
	}
    }
  closedir(dirp);
  return (0);
}

int     list_dir(char *dir, int a)
{
  DIR	*dirp;

  dirp = opendir(dir);
  if (dirp == 0)
    {
      my_putstr("Error !");
      return (1);
    }
  while ((pdir = readdir(dirp)))
    {
      if (a == 0)
	{
	  my_putstr(pdir->d_name);
	  my_putstr("  ");
	}
      else if (my_strncmp(pdir->d_name, ".", 1) == -1)
	{
	  my_putstr(pdir->d_name);
	  my_putstr("  ");
	}
    }
  closedir(dirp);
  return (0);
}

int	main(int ac, char **av)
{
  list_dir(av[1], 1);
  my_putchar('\n');
  return (0);
}
