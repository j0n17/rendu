/*
** main.c for main.c in /home/maire_j/rendu/Bistromathique-2017-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Nov  9 21:35:49 2013 Maire Jonathan
** Last update Sun Nov 10 02:39:49 2013 Maire Jonathan
*/

int	main(int ac, char **av)
{
  if (ac == 3)
    {
      my_putstr(my_add(av[1], av[2]));
    }
  return (0);
}
