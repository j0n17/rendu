/*
** add.c in /home/maire_j/rendu/Bistromathique-2017-maire_j
**
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
**
** Started on  Mon Nov  4 14:02:11 2013 Maire Jonathan
** Last update Sun Nov 10 02:45:27 2013 Maire Jonathan
*/

#include <stdlib.h>
#include <my.h>

int	larger(char *nb1, char *nb2)
{
  if (my_strlen(nb1) > my_strlen(nb2))
    return(my_strlen(nb1) - 1);
  else
    return (my_strlen(nb2) - 1);
}

int	check_neg_sub(char *nb1, char *nb2)
{
  if (my_strncmp(nb1, "-", 1) == 0)
    {
      /*my_sub(nb1, nb2);*/
      return (1);
    }
  else if (my_strncmp(nb2, "-", 1) == 0)
    {
      /*my_sub(nb2, nb1);*/
      return (1);
    }
  return (0);
}

char	*new_nb(char *nb, int size)
{
  char	*n_nb;
  int	i;
  int	j;

  i = 0;
  j = my_strlen(nb) - 1;
  if ((n_nb = malloc(size + 1)) != 0)
    {
      while (i < size + 1)
	{
	  n_nb[i] = '0';
	  i = i + 1;
	}
      while (j >= 0)
	{
	  n_nb[i] = nb[j];
	  j = j - 1;
	  i = i - 1;
	}
      return (n_nb);
    }
  return (0);
}

int	check_carry(int *carry, int *result)
{
  *carry = 0;
  if (*result >= 10)
    {
      *carry = *result / 10;
      *result = *result % 10;
    }
  return (*result);
}

char	*my_add(char *nb1, char *nb2)
{
  char	*st_res;
  char	*n1;
  char	*n2;
  int	i;
  int	carry;
  int	result;

  i = 0;
  carry = 0;
  result = 0;
  if (check_neg_sub(nb1, nb2) == 1)
    return (0);
  if ((st_res = malloc(larger(nb1, nb2) + 1)) == 0)
    return (0);
  n1 = my_revstr(new_nb(nb1, larger(nb1, nb2)));
  n2 = my_revstr(new_nb(nb2, larger(nb1, nb2)));
  while (n1[i] && n2[i])
    {
      result = n1[i] + n2[i] - 96 + carry;
      st_res[i] = check_carry(&carry, &result) + 48;
      i = i + 1;
    }
  free(n1);
  free(n2);
  return (my_revstr(st_res));
}

