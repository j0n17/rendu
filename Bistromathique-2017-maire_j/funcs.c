/*
** funcs.c for funcs.c in /home/maire_j/rendu/Bistromathique-2017-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Nov  9 21:44:59 2013 Maire Jonathan
** Last update Sat Nov  9 22:50:10 2013 Maire Jonathan
*/

void	my_rev_str(char *str)
{
  char	tmp;
  int	i;
  int	k;

  k = 0;
  i = my_strlen(str) - 1;
  while (i >= my_strlen(str) / 2)
    {
      tmp = str[k];
      str[k] = str[i];
      str[i] = tmp;
      k = k + 1;
      i = i - 1;
    }
}

void	empty_buff(char *str, int size)
{
  int	i;

  i = 0;
  while (i < size)
    {
      str[i] = '0';
      i = i + 1;
    }
  str[i] = '\0';
}
