/*
** eval_expr.h for eval_expr in /home/robert_u/rendu/Piscine-C-include
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 16:19:40 2013 robert de saint vincent
** Last update Tue Nov  5 17:16:16 2013 tho
*/


#ifndef EVAL_EXPR_H_
# define EVAL_EXPR_H_
# define SYNTAXE_ERROR_MSG	"syntax error\n"

/*******************main***************/
void		eval_expr(char *, char *, char *, unsigned int);
char		*mainloop(char **tab, char *ops);
void            check_base(char *b);
void		check_ops(char *ops);
char            *get_expr(unsigned int size);
/*************my_ops_in_tab************/
char		**put_str_in_tab(char *str, char **tab, char *ops);
void		aff_double_tab(char **tab);
char		**rabot_tab(char **tab, int i, int j);
int		my_tab_size(char **tab);
/****************do_my_ops*************/
int		do_parent_ops(char **tab, char *ops);
int		do_priority_ops(char **tab, char *ops);
int		do_low_ops(char **tab, char *ops);
/**************support_eval************/
char		*remove_spaces(char *str);
int		is_operator(char c, char *ops);
int		my_digits(int nb);
char		*my_int_to_str(int nb);
int		my_calc(int n1, char op, int n2, char *ops);
/******************ops******************/
int		my_plus(int nb1, int nb2);
int		my_less(int nb1, int nb2);
int		my_time(int nb1, int nb2);
int		my_divi(int nb1, int nb2);
int		my_modu(int nb1, int nb2);
/****************my_errors******************/
void            check_base(char *base);
void            check_ops(char *ops);
void            check_expr(char *expr, char *ops, int i, int size);
/****************my_add********************/
/****************my_substract**************/
/****************my_mult*******************/
/****************my_div********************/
/****************my_mod********************/
#endif /*EVAL_EXPR_H_*/
