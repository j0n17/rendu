/*
** my_swap.c for swap in /home/robert_u/rendu/Piscine-C-Jour_03
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Thu Oct  3 09:34:29 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 11:36:17 2013 thomas-robert-de-saint-vincent
*/

int	my_swap(int *a, int *b)
{
  int	temp;

  temp = *a;
  *a = *b;
  *b = temp;
  return (0);
}
