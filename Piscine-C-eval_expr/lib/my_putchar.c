/*
** my_putchar.c for my_putchar in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 11:11:34 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 11:12:24 2013 thomas-robert-de-saint-vincent
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}
