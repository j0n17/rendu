/*
** my_strcpy.c for my_strcpy in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 09:16:41 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:40:33 2013 thomas-robert-de-saint-vincent
*/

char	*my_revstr(char *str)
{
  int	i;
  int	size;
  char	temp;

  size = 0;
  i = 0;
  while (str[size])
    {
      size = size + 1;
    }
  size = size - 1;
  while (i < (size / 2) + size % 2)
    {
      temp = str[i];
      str[i] = str[size - i];
      str[size - i] = temp;
      i = i + 1;
    }
  return (str);
}
