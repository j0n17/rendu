/*
** my_strlowcase.c for strlowcase in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 19:01:28 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 11:24:11 2013 thomas-robert-de-saint-vincent
*/

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	{
	  str[i] = str[i] + 32;
	}
      i = i + 1;
    }
  return (str);
}
