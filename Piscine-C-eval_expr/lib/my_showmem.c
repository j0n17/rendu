/*
** my_showmem.c for my_showmem in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 17:41:56 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 17:17:34 2013 thomas-robert-de-saint-vincent
*/

void	adress(char *str, int i)
{
  my_put_nbr(to_exa(&str[i]));
  my_putstr(": ");
}

void	exa(char *str, int i, int size)
{
  int	cont;

  cont = 1;
  while (cont < 16)
    {
      if (size > 0 && str[i + cont])
	{
	  if (str[i + cont] < 16)
	    {
	      my_putchar('0');
	    }
	  my_putchar(to_exa(str[i + cont]));
	}
      else
	{
	  my_putstr("  ");
	}
      if (cont % 2 == 1)
	{
	  my_putchar(' ');
	}
      cont = cont + 1;
      size = size - 1;
    }
}

void	string(char *str, int i, int size)
{
  int	cont;

  cont = 0;
  my_putchar(' ');
  while (cont < 16 && str[i + cont] && size > 0)
    {
      if (str[i + cont] >= ' ' && str[i + cont] <= '~')
	{
	  my_putchar(str[i + cont]);
	}
      else
	{
	  my_putchar('.');
	}
      cont = cont + 1;
      size = size - 1;
    }
}

int	my_showmem(char *str, int size)
{
  int	i;

  i = 0;
  while (size > 0 &&  i < my_strlen(str))
    {
      if ( i != 0 && i % 16 == 0)
	{
	  my_putstr("\n");
	}
      adress(str, i);
      exa(str, i, size);
      string(str, i, size);
      i = i + 16;
      size = size - 16;
    }
  return (0);
}
