/*
** my_strupcase.c for strupcase in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 18:48:56 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 11:23:28 2013 thomas-robert-de-saint-vincent
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	{
	  str[i] = str[i] - 32;
	}
      i = i + 1;
    }
  return (str);
}
