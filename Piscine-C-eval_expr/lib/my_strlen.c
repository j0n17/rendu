/*
** my_putstr.c for my_putnbr in /home/robert_u/rendu/Piscine-C-Jour_04
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Thu Oct  3 13:59:03 2013 thomas-robert-de-saint-vincent
** Last update Fri Oct  4 20:41:16 2013 thomas-robert-de-saint-vincent
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
