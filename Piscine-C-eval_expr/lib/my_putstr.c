/*
** my_putstr.c for my_putnbr in /home/robert_u/rendu/Piscine-C-Jour_04
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Thu Oct  3 13:59:03 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 16:44:07 2013 thomas-robert-de-saint-vincent
*/

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i = i + 1;
    }
  return (0);
}

