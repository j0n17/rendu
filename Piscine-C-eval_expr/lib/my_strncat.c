/*
** my_strncat.c for strncat in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 11:51:55 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 17:18:49 2013 thomas-robert-de-saint-vincent
*/

#include<string.h>
char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  int	src_taille;
  int	dest_taille;

  i = 0;
  src_taille = my_strlen(src);
  dest_taille = my_strlen(dest);
  while (i < src_taille && i < nb)
    {
      dest[dest_taille + i] = src[i];
      i = i + 1;
    }
  dest[dest_taille + i] = '\0';
  return (dest);
}
