/*
** my_strncpy.c for strncpy in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 10:03:25 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 20:22:11 2013 thomas-robert-de-saint-vincent
*/

char	*my_strncpy( char *dest, char *src, int n)
{
  int	i;
  int	taille;

  i = 0;
  while (src[i])
    {
      if ( i < n)
	{
	  dest[i] = src[i];
	}
      else
	{
	  dest[i] = '\0';
	}
      i = i + 1;
    }
  return (dest);
}
