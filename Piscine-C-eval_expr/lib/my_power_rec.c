/*
** my_power_rec.c for my_power_rec in /home/robert_u/rendu/Piscine-C-Jour_05
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Fri Oct  4 11:06:54 2013 thomas-robert-de-saint-vincent
** Last update Thu Oct 10 22:41:07 2013 thomas-robert-de-saint-vincent
*/

int	my_power_rec(int nb, int power)
{
  if (power == 0)
    {
      return (1);
    }
  if (power > 1)
    {
      return (nb * my_power_rec(nb, (power - 1)));
    }
}
