/*
** my_strcapitalyze.c for strcapitalize in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 19:07:19 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:44:53 2013 thomas-robert-de-saint-vincent
*/

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	{
	  str[i] = str[i] + 32;
	}
      if (i == 0 && str[i] >= 'a' && str[i] <= 'z')
	{
	  str[i] = str[i] - 32;
	}
      else if (str[i - 1] && str [i] >= 'a' && str[i] <= 'z' &&
	       ((str[i - 1] < '0' && str[i - 1] >= ' ') ||
		(str[i - 1] > '9' && str[i - 1] < 'A')))
	{
	  str[i] = str[i] - 32;
	}
	i = i + 1;
    }
  return (str);
}
