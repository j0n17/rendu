/*
** my_str_isnum.c for my_str_isnum in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 20:40:10 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:46:15 2013 thomas-robert-de-saint-vincent
*/

char	my_str_isnum(char *str)
{
  int	i;
  int	result;

  i = 0;
  result = 1;
  while (str[i] && result == 1)
    {
      if (str[i] < '0' || str[i] > '9')
	{
	  result = 0;
	}
      i = i + 1;
    }
  return (result);
}
