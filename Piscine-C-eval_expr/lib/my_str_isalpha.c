/*
** my_str_isalpha.c for str_isalpha in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 20:24:48 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:45:48 2013 thomas-robert-de-saint-vincent
*/

char	my_str_isalpha(char *str)
{
  int	i;
  int	result;

  i = 0;
  result = 1;
  while (str[i] && result == 1)
    {
      if (str[i] < 'A' || str[i] > 'z' || (str[i] > 'Z' && str[i] < 'a'))
	{
	  result = 0;
	}
      i = i + 1;
    }
  return (result);
}
