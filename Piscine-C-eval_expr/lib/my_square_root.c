/*
** my_square_root.c for my_square_root in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 15:26:52 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 16:51:46 2013 thomas-robert-de-saint-vincent
*/

int	my_square_root(int nb)
{
  int	i;
  int	result;

  i = 1;
  result = 0;
  while (i <= nb / 2 + 1 && result == 0)
    {
      if ( i * i == nb)
	{
	  result = i;
	}
      else
	{
	  i = i + 1;
	}
    }
  return (result);
}
