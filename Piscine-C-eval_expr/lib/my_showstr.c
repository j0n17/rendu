/*
** my_showstr.c for my_showstr in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 15:57:28 2013 thomas-robert-de-saint-vincent
** Last update Fri Oct 11 11:34:55 2013 thomas-robert-de-saint-vincent
*/

int	to_exa(int nb)
{
  int	i;
  char	table[16];

  i = 0;
  while (i < 16)
    {
      if (i <= 9)
        {
          table[i] = i + 48;
        }
      else
        {
          table[i] = i + 48 + 7;
        }
      i = i + 1;
    }
  if (nb < 1)
    {
      return (nb);
    }
  to_exa((nb / 16));
  my_putchar(table[nb % 16]);
  return (0);
}

int	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 32 && str[i] <= 126)
	{
	  my_putchar(str[i]);
	}
      else
	{
	  my_putchar('\\');
	  if (str[i] < 16)
	    {
	      my_putchar('0');
	    }
	  to_exa(str[i]);
	}
      i = i + 1;
    }
  return (0);
}
