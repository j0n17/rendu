/*
** my_put_nbr.c for put_nbr in /home/robert_u/rendu/Piscine-C-Jour_03
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Wed Oct  2 11:05:55 2013 thomas-robert-de-saint-vincent
** Last update Fri Oct 11 19:00:16 2013 thomas-robert-de-saint-vincent
*/

int	my_put_nbr(int nb)
{
  if (nb -1 > nb || nb + 1 < nb)
    {
      my_putchar(48);
    }
  else
    {
      if (nb < 0)
	{
	  my_putchar('-');
	  nb = 0 - nb;
	}
      if (nb >= 10)
	{
	  my_put_nbr(nb / 10);
	}
      my_putchar((nb % 10) + 48);
    }
  return (0);
}
