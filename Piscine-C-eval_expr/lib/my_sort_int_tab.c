/*
** my_sort_in_tab.c for my_sort_in_tab in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 10:22:43 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 14:43:47 2013 thomas-robert-de-saint-vincent
*/

void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (i <= size && tab[i])
    {
      while (j <= size - 1)
	{
	  if (tab[j - 1] && tab[j - 1] > tab[j])
	    {
	      my_swap(&tab[j - 1], &tab[j]);
	    }
	  j = j + 1;
	}
      j = 0;
      i = i + 1;
    }
}
