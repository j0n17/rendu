/*
** my_strcat.c for my_strcat in /home/caniau_q/rendu/Piscine-C-lib/my
** 
** Made by 
** Login   <caniau_q@epitech.net>
** 
** Started on  Tue Oct  8 10:21:55 2013 
** Last update Wed Oct 30 17:10:41 2013 tho
*/
#include<stdlib.h>

char	*my_strcat(char *dest, char *scr)
{
  int	i;
  int	j;
  int	dest_size;
  int	scr_size;
  char	*str;

  i = 0;
  j = 0;
  dest_size = my_strlen(dest);
  scr_size = my_strlen(scr);
  str = malloc(sizeof(char) * (scr_size + dest_size + 2));
  while (dest[i])
    {
      str[i] = dest[i];
      i = i + 1;
    }
  while (scr[j])
    {
      str[i] = scr[j];
      i = i + 1;
      j = j + 1;
    }
  str[i + 1] = '\0';
  return (str);
}
