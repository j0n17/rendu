/*
** my_isneg.c for isneg in /home/robert_u/rendu/Piscine-C-Jour_03
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Wed Oct  2 10:25:41 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 16:32:21 2013 thomas-robert-de-saint-vincent
*/

int	my_isneg(int n)
{
  if (n >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('N');
    }
  return (0);
}
