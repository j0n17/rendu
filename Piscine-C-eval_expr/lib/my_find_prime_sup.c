/*
** my_find_prime_sup.c for my_find_prime_sup in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 15:16:21 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 15:21:04 2013 thomas-robert-de-saint-vincent
*/
int	my_find_prime_sup(int nb)
{
  int	result;

  result = 0;
  while (result == 0)
    {
      if (my_is_prime(nb) == 1)
	{
	  result = nb;
	}
      else
	{
	  nb = nb + 1;
	}
    }
  return (nb);
}
