/*
** my_strcpy.c for my_strcpy in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 09:16:41 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 20:24:35 2013 thomas-robert-de-saint-vincent
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}
