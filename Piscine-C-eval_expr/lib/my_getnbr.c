/*
** my_getnbr.c for getnbrv2 in /home/robert_u/rendu/Piscine-C-Jour_04
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Thu Oct  3 19:42:17 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 21:27:33 2013 thomas-robert-de-saint-vincent
*/



int     my_strlen4(char *str)
{
  int   i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}

long	boucle(char *str, int i, long power, long nbe)
{
  while (i >= 0)
    {
      if (str[i] >= 48 && str[i] <= 57)
	{
	  nbe = nbe + power * (str[i] - 48);
	  power = power * 10;
	}
      else if (str[i] == '+' || str[i] == '-')
	{
	  if (str[i] == '-')
	    {
	      nbe = 0 - nbe;
	    }
	}
      else
	{
	  power = 1;
	  nbe = 0;
	}
      i = i - 1;
    }
  return (nbe);
}

long	calc(char *str, int i, long power, long nbe)
{
  nbe = boucle(str, i, power, nbe);
  if (nbe >= -2147483648 && nbe <= 2147483647)
    {
      return ((int)nbe);
    }
  else
    {
      return (0);
    }
}

int	my_getnbr(char *str)
{
  int	i;
  long	power;
  long	nbe;

  i = my_strlen4(str);
  power = 1;
  nbe = 0;
  return (calc(str, i, power, nbe));
}
