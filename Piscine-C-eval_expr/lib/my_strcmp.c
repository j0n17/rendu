/*
** my_strcmp.c for strcmp in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 17:59:33 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:43:04 2013 thomas-robert-de-saint-vincent
*/

int	my_strcmp(char *s1, char *s2)
{
  int	result;
  int	i;

  i = 1;
  result = 1;
  while (s1[i] && result == 1)
    {
      if (s1[i] != s2[i])
	{
	  result = 0;
	}
      i = i + 1;
    }
  i = i - 1;
  return (s1[i] - s2[i]);
}
