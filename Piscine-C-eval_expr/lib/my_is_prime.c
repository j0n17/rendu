/*
** my_is_prime.c for my_is_prime in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 14:55:19 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 16:52:59 2013 thomas-robert-de-saint-vincent
*/
int	my_is_prime(int nb)
{
  int	result;
  int	i;

  i = 2;
  result = 1;
  while (i < nb && result == 1)
    {
      if (nb % i == 0)
	{
	  result = 0;
	}
      i = i + 1;
    }
  return (result);
}
