/*
** my_str_islower.c for my_str_islower in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 20:42:05 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:46:28 2013 thomas-robert-de-saint-vincent
*/

char	my_str_islower(char *str)
{
  int	i;
  int	result;

  i = 0;
  result = 1;
  while (str[i] && result == 1)
    {
      if (str[i] < 'a' || str[i] > 'z')
	{
	  result = 0;
	}
      i = i + 1;
    }
  return (result);
}
