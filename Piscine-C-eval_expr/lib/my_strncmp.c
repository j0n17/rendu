/*
** my_strncmp.c for strncmp in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 18:27:03 2013 thomas-robert-de-saint-vincent
** Last update Mon Oct  7 23:43:12 2013 thomas-robert-de-saint-vincent
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int	result;
  int	i;

  i = 0;
  result = 1;
  while ((s1[i]) && (i <= n) && result == 1)
    {
      if (s1[i] != s2[i])
	{
	  result = 0;
	}
      i = i + 1;
    }
  i = i - 1;
  return (s1[i] - s2[i]);
}
