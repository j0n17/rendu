/*
** my_strstr.c for strstr in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 17:03:17 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 16:59:02 2013 thomas-robert-de-saint-vincent
*/

int	my_strln4(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      i = i + 1;
    }
  return (i - 1);
}

int	entier(char *str, char *to_find, int i)
{
  int	j;
  int	result;

  j = 0;
  result = 1;
  while (to_find[j])
    {
      if (str[i] != to_find[j])
	{
	  result = 0;
	}
      i = i + 1;
      j = j + 1;
    }
  return (result);
}

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	return1;

  i = 0;
  return1 = 0;
  while (str[i] && return1 == 0)
    {
      if (str[i] == to_find[0] && entier(str, to_find, i) == 1)
	{
	  return1 = 1;
	  break;
	}
      i = i + 1;
    }
  if (return1 == 1)
    {
      return (str + i);
    }
  else
    {
      return (0);
    }
}
