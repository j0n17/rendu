/*
** my_strlcat.c for strlcat in /home/robert_u/rendu/Piscine-C-lib/my
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Oct  8 12:08:34 2013 thomas-robert-de-saint-vincent
** Last update Wed Oct  9 11:03:42 2013 thomas-robert-de-saint-vincent
*/

int	my_strlcat(char *dest, char *src, int size)
{
  if (size <= my_strlen(dest))
    {
      return (size + my_strlen(src));
    }
  else
    {
      return (my_strlen(dest) + my_strlen(src));
    }
}
