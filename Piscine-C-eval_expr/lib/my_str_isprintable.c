/*
** my_str_isprintable.c for my_str_isprintable in /home/robert_u/rendu/Piscine-C-Jour_06
** 
** Made by thomas-robert-de-saint-vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Mon Oct  7 21:22:43 2013 thomas-robert-de-saint-vincent
** Last update Tue Oct  8 16:28:26 2013 thomas-robert-de-saint-vincent
*/

char	my_str_isprintable(char *str)
{
  int	i;
  int	result;

  i = 0;
  result = 0;
  while (str[i] && result == 0)
    {
      if (str[i] >= 33 && str[i] <= 126)
	{
	  result = 1;
	}
      i = i + 1;
    }
  return (result);
}
