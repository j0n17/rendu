/*
** eval_expr.c for e-C-eval_expr in /home/robert_u/rendu/Piscine-C-eval_expr
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 11:03:42 2013 robert de saint vincent
** Last update Tue Nov  5 17:34:26 2013 tho
*/
#include "eval_expr.h"
#include "my.h"
#include <stdlib.h>

char		*mainloop(char **tab, char *ops)
{
  int		success;
  int		j;

  success = 1;
  while (success == 1)
    {
      success = do_parent_ops(tab, ops);
      if (success == 0)
      	success = do_priority_ops(tab, ops);
      if (success == 0)
	success = do_low_ops(tab,ops);
      //aff_double_tab(tab);
      //my_putstr("\n");
    }
  my_putstr(tab[0]);
  return (0);
}

void		eval_expr(char *base, char *ops, char *expr, unsigned int size)
{
  int		i;
  int		j;
  char		**tab;

  expr = remove_spaces(expr);
  tab = malloc(sizeof(char *) * size + 1);
  if (tab == NULL)
    my_putstr("fail de malloc");
  i = 0;
  while (i < size + 1)
    {
      tab[i] = malloc(sizeof(char) * size + 1);
      i = i + 1;
    }
  put_str_in_tab(expr, tab, ops);
  mainloop(tab, ops);
  free(tab);
}

int		main(int ac, char **av)
{
  char		*expr;

  if (ac != 4)
   {
      my_putstr("Usage : ");
      my_putstr(av[0]);
      my_putstr(" base ops\"()+-*/%\" exp_len\n");
      return (0);
    }
  check_base(av[1]);
  check_ops(av[2]);
  expr = get_expr(my_getnbr(av[3]));
  check_expr(expr, av[2], 0, my_getnbr(av[3]));
  eval_expr(av[1], av[2], expr, my_getnbr(av[3]));

  return (0);
}

char		*get_expr(unsigned int size)
{
  char		*expr;

  if (size <= 0)
    {
      my_putstr("Bad expr len\n");
      exit(2);
    }
  expr = malloc(size+1);
  if (expr == 0)
    {
      my_putstr("could not alloc\n");
      exit(3);
    }
  if (read(0, expr, size) != size)
    {
      my_putstr("could not read\n");
      exit(4);
    }
  expr[size] = 0;
  return (expr);
}
