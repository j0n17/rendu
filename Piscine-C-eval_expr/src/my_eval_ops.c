/*
** my_eval_ops.c for my_eval_ops in /home/robert_u/rendu/Piscine-C-eval_expr
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 20:46:27 2013 robert de saint vincent
** Last update Sun Oct 27 20:51:19 2013 robert de saint vincent
*/
#include "eval_expr.h"
int             my_plus(int nb1, int nb2)
{
  return (nb1 + nb2);
}

int             my_less(int nb1, int nb2)
{
  return (nb1 - nb2);
}

int             my_time(int nb1, int nb2)
{
  return (nb1 * nb2);
}

int             my_divi(int nb1, int nb2)
{
  return (nb1 / nb2);
}

int             my_modu(int nb1, int nb2)
{
  return (nb1 % nb2);
}
