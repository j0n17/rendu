/*
** my_errors.c for my_errors in /home/robert_u/rendu/Piscine-C-eval_expr/src
** 
** Made by tho
** Login   <robert_u@epitech.net>
** 
** Started on  Tue Nov  5 12:29:18 2013 tho
** Last update Tue Nov  5 19:44:00 2013 tho
*/
#include "eval_expr.h"
#include "my.h"
#include <stdlib.h>


void		check_ops(char *ops)
{
  if (my_strlen(ops) != 7)
    {
      my_putstr("Bad ops\n");
      exit(5);
    }
}

void		check_base(char *base)
{
  if (my_strlen(base) < 2)
    {
      my_putstr("Bad base\n");
      exit(1);
    }
}

void		check_expr(char *expr, char *ops, int i, int size)
{
  int		parents;

  parents = 0;
  while (i < size)
    {
      if (expr[i] == ops[0])
	parents = parents + 1;
      else if (expr[i] == ops[1])
	parents = parents - 1;
      i = i + 1;
    }
  while (i > 0)
    {
      if (0)
	{
	  my_putstr(SYNTAXE_ERROR_MSG);
	  exit(6);
	}
      i = i - 1;
    }
  if (parents != 0)
    {
      my_putstr(SYNTAXE_ERROR_MSG);
      exit(7);
    }
}
