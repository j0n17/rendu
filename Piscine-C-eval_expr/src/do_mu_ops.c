/*
** do_mu_ops.c for my_ops in /home/robert_u/rendu/Piscine-C-eval_expr
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 22:45:56 2013 robert de saint vincent
** Last update Tue Nov  5 14:24:10 2013 tho
*/
#include "eval_expr.h"

int		do_parent_ops(char **tab, char *ops)
{
  int		i;
  int		success;

  i = 0;
  success = 0;
  while (my_tab_size(tab) > 3 && i < my_tab_size(tab) && success == 0)
    {
      if (tab[i] && tab[i][0] == ops[1] && tab[i - 4] &&
	  tab[i - 4][0] == ops[0] && is_operator(tab[i - 2][0], ops) < 3)
	{
	  tab[i - 4] = my_int_to_str(my_calc(my_getnbr(tab[i - 3]),
	  				     tab[i - 2][0], my_getnbr(tab[i - 1]), ops));
	  rabot_tab(tab, i - 3, 4);
	  success = 1;
	}
      i = i + 1;
    }
  return (success);
}

int		do_priority_ops(char **tab, char *ops)
{
  int		i;
  int		success;

  i = 0;
  success = 0;
  while (i < my_tab_size(tab) && success == 0)
    {
      if (is_operator(tab[i][0], ops) == 1 && tab[i - 1] &&
	  is_operator(tab[i - 1][0], ops) == -1 && tab[i + 1] &&
	  is_operator(tab[i + 1][0], ops) == -1)
	{
	  tab[i - 1] = my_int_to_str(my_calc(my_getnbr(tab[i - 1]),
	  				     tab[i][0], my_getnbr(tab[i + 1]), ops));
	  rabot_tab(tab, i, 2);
	  success = 1;
	}
      i = i + 1;
    }
  return (success);
}
int		do_low_ops(char **tab, char *ops)
{
  int		i;
  int		success;

  i = 0;
  success = 0;
  while (i < my_tab_size(tab) && success == 0)
    {
      if (is_operator(tab[i][0], ops) == 0
	  && tab[i - 1] && is_operator(tab[i - 1][0], ops) == -1
	  && tab[i + 1] && is_operator(tab[i + 1][0], ops) == -1)
	{
	  tab[i - 1] = my_int_to_str(my_calc(my_getnbr(tab[i - 1]),
	  				     tab[i][0], my_getnbr(tab[i + 1]), ops));
	  rabot_tab(tab, i, 2);
	  success = 1;
	}
      i = i + 1;
    }
  return (success);
}
