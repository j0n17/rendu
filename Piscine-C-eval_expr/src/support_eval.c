/*
** support_eval.c for support_eval in /home/robert_u/rendu/Piscine-C-eval_expr
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 18:08:39 2013 robert de saint vincent
** Last update Tue Nov  5 14:30:15 2013 tho
*/
#include "eval_expr.h"
#include <stdlib.h>

int		my_digits(int nb)
{
  int		i;

  i = 0;
  while (nb != 0)
    {
      i = i + 1;
      nb = nb / 10;
    }
  return (i);
}

char		*my_int_to_str(int nb)
{
  char		*str;
  int		d;
  int		i;

  i = 0;
  str = malloc(sizeof(char) * (my_digits(nb) + 1));
  if (nb < 0)
    {
      str[my_digits(nb) + 1] == '-';
      nb = -nb;
    }
  while (nb > 0)
    {
      str[i] = nb % 10 + 48;
      i = i + 1;
      nb = nb / 10;
    }
  my_revstr(str);
  return (str);
}

char		*remove_spaces(char *str)
{
  int		i;
  int		j;

  i = 0;
  j = 0;
  while (str[i] && str[j])
    {
      if (str[i] != ' ')
	{
	  str[j] = str[i];
	  j = j + 1;
	}
      i = i + 1;
    }
  str[j] = '\0';
  return (str);
}

int		my_calc(int n1, char op, int n2, char *ops)
{
  int		(*calc[11])(int n1, int n2);
  int		i;

  calc[2]=my_plus;
  calc[3]=my_less;
  calc[4]=my_time;
  calc[5]=my_divi;
  calc[6]=my_modu;
  i = 2;
  while (i < 7)
    {
      if (op == ops[i])
	return (calc[i](n1, n2));
      i = i + 1;
    }
  return (0);
}

int		is_operator(char c, char *ops)
{
  if (c == ops[0] || c == ops[1])
    {
      return (2);
    }
  else if (c == ops[6] || c == ops[5] || c == ops[4])
    {
      return (1);
    }
  else if (c == ops[2] || c == ops[3])
    {
      return (0);
    }
  else
    {
      return (-1);
    }
}
