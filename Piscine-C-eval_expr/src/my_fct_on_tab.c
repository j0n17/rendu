/*
** my_fct_on_tab.c for my_ops_on_tab in /home/robert_u/rendu/Piscine-C-eval_expr
** 
** Made by robert de saint vincent
** Login   <robert_u@epitech.net>
** 
** Started on  Sun Oct 27 18:37:13 2013 robert de saint vincent
** Last update Tue Nov  5 14:08:21 2013 tho
*/

char		**put_str_in_tab(char *str, char **tab, char *ops)
{
  int		i;
  int		j;
  int		k;

  i = 0;
  j = 0;
  k = 0;
  while (str[i])
    {
      tab[j][k] = str[i];
      k = k + 1;
      i = i + 1;
      if (is_operator(str[i], ops) >= 0 || (str[i - 1] && str[i] &&
				       is_operator(str[i - 1], ops) >= 0))
        {
	  tab[j][k] = '\0';
          k = 0;
          j = j + 1;
        }
    }
  return (tab);
}

void		aff_double_tab(char **tab)
{
  int		i;
  int		j;

  i = 0;
  while (tab[i][0] != '\0')
    {
      j = 0;
      while (tab[i][j] != '\0')
	{
	  my_putchar(tab[i][j]);
	  j = j + 1;
	}
      my_putchar(' ');
      i = i + 1;
    }
}

char		**rabot_tab(char **tab, int i, int j)
{
  while (tab[i] && tab[i + j])
    {
      tab[i] = tab[i + j];
      i = i + 1;
    }
  return (tab);
}

int		my_tab_size(char **tab)
{
  int		i;

  i = 0;
  while (tab[i][0] != '\0')
    {
      i = i +1;
    }
  return (i);
}
