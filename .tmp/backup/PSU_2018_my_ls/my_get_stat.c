/*
** my_get_stat in /home/maire_j/Desktop/myls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 31 14:48:08 2013 Maire Jonathan
** Last update Sun Nov  3 23:32:07 2013 Maire Jonathan
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>

int	my_get_size(char *file)
{
  int	size;
  struct stat   st;

  size = -1;
  if (stat(file, &st) == 0)
    {
      size = st.st_size;
    }
  return (size);
}

int     my_get_inode(char *file)
{
  int   inode;
  struct stat   st;

  inode = -1;
  if (stat(file, &st) == 0)
    {
      inode = st.st_ino;
    }
  return (inode);
}

void	my_get_guid(char *file, int i)
{
  int	guid;
  struct stat	st;
  struct passwd	*p;
  struct group	*g;

  guid = 0;
  p = NULL;
  g = NULL;
  if (i == 0)
    {
      if (stat(file, &st) == 0)
	{
	  guid = st.st_uid;
	  p = getpwuid(guid);
	  my_putstr(p->pw_name);
	}
    }
  else if (i == 1)
    {
      if (stat(file, &st) == 0)
        {
          guid = st.st_gid;
	  g = getgrgid(guid);
	  my_putstr(g->gr_name);
        }
    }
}

void	my_get_mtime(char *file)
{
  time_t	time_epoch;
  struct tm	*mtime;
  struct stat	st;

  if (stat(file, &st) == 0 && st.st_mtime != -1)
    {
      time_epoch = st.st_mtime;
    }
  mtime = localtime(&time_epoch);
  my_putstr("  ");
  my_put_nbr(mtime->tm_mday);
  my_putchar(' ');
  my_put_nbr(mtime->tm_hour);
  my_putchar(':');
  my_put_nbr(mtime->tm_min);
}
