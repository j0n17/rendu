/*
** args.c for args in /home/maire_j/rendu/PSU_2013_my_ls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov  3 16:00:52 2013 Maire Jonathan
** Last update Sun Nov  3 16:12:22 2013 Maire Jonathan
*/

#include "libmy/my.h"
#include <dirent.h>

void	proc_args_files(char **flist, char *args)
{
  my_putstr(args);
  my_putchar('\n');
  my_putchar('a');
}

void	proc_args(char *args)
{
  my_putstr(args);
  my_putchar('\n');
}
