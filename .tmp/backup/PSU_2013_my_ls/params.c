/*
** params.c for params in /home/maire_j/rendu/PSU_2013_my_ls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov  3 12:17:21 2013 Maire Jonathan
** Last update Sun Nov  3 15:59:45 2013 Maire Jonathan
*/

#include <stdlib.h>
#include <dirent.h>
#include "libmy/my.h"

int	count_flist(int ac, char **av)
{
  int	i;
  int	res;

  i = 1;
  res = 0;
  while (i < ac)
    {
      if (av[i][0] != '-' && av[i][0] != '\0')
	res = res + 1;
      i = i + 1;
    }
  return (res);
}

void	process_allargs(int ac, char **av, char *plist)
{
  int	i;
  int	j;
  int	f_size;
  char	**flist;

  i = 0;
  j = 0;
  f_size = count_flist(ac, av);
  if (f_size > 0)
    {
      if ((flist = malloc(f_size * sizeof(char *))) != NULL)
	{
	  while (i < ac)
	    {
	      if (av[i][0] != '-' && av[i][0] != '\0')
		{
		  flist[j] = my_strdup(av[i]);
		  j = j + 1;
		}
	      i = i + 1;
	    }
	}
      proc_args_files(flist, plist);
    }
  else
    proc_args(plist);
}

void	process_all(int ac, char **av)
{
  int	f_size;
  char	**flist;
  int	i;
  int	j;

  i = 1;
  j = 0;
  f_size = count_flist(ac, av);
  if (f_size != 0)
    {
      if ((flist = malloc(f_size * sizeof(char *))) != NULL)
	{
	  while (i < ac)
	    {
	      if (av[i][0] != '-')
		print_dir(av[i]);
	      i = i + 1;
	    }
	}
      else
	my_putstr("Memory ALlocation error !\n");
    }
  else
    print_dir(".");
}

int	print_dir(char *dir)
{
  DIR	*dirp;
  struct dirent	*d;

  dirp = opendir(dir);
  if (dirp == 0)
    {
      my_putstr("Cannot access ");
      my_putstr(dir);
      my_putstr(": No such file or directory\n");
      return (1);
    }
  while ((d = readdir(dirp)) != 0)
    {
      if (my_strncmp(d->d_name, ".", 1) != 0)
	{
	  my_putstr(d->d_name);
	  my_putchar('\n');
	}
    }
  return (0);
}
