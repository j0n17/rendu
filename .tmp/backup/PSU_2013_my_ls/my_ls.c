/*
** my_ls.c for my_ls in /home/maire_j/rendu/myls-2013-maire_j
**
-** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
**
** Started on  Mon Oct 28 11:54:07 2013 Maire Jonathan
** Last update Sun Nov  3 16:25:25 2013 Maire Jonathan
*/

#include "myls.h"
#include "libmy/my.h"
#include <dirent.h>
#include <stdlib.h>

int	count_param(int ac, char **av)
{
  int	i;
  int	res;

  i = 0;
  res = 0;
  while (i < ac)
    {
      if (av[i][0] == '-')
	{
	  res = res + my_strlen(av[i]) - 1;
	}
      i = i + 1;
    }
  return (res);
}

char	*get_params(int ac, char **av, char *list)
{
  int	i;
  int	lspos;
  int	k;

  i = 0;
  lspos = 0;
  k = 1;
  while (i < ac)
    {
      if (av[i][0] == '-')
	{
	  while (av[i][k])
	    {
	      list[lspos] = av[i][k];
	      lspos = lspos + 1;
	      k = k + 1;
	    }
	  k = 1;
	}
      i = i + 1;
    }
  return(list);
}

int	check_p(char *list, char *verif)
{
  int	i;
  int	j;
  int	res;

  i = 0;
  j = 0;
  res = 1;
  while (list[i])
    {
      res = 1;
      while (verif[j])
	{
	  if (list[i] == verif[j])
	    {
	      res = 0;
    	    }
	  j = j + 1;
	}
      if (res == 1)
	{
	  return (1);
	}
      j = 0;
      i = i + 1;
    }
  return (0);
}

int	my_ls(int ac, char **av)
{
  int	pnb;
  char	plist_v[6];
  char	*plist;

  plist_v[0] = 'l';
  plist_v[1] = 'R';
  plist_v[2] = 'd';
  plist_v[3] = 'r';
  plist_v[4] = 't';
  pnb = count_param(ac, av);
  if (pnb > 0)
    {
      plist = malloc(sizeof(char) * pnb);
      if (plist == NULL)
	{
	  my_putstr("Memory ALLOCation error !\n");
	  return (1);
	}
      plist = get_params(ac, av, plist);
      if (check_p(plist, plist_v) == 1)
	{
	  my_putstr("Invalid option, available options are [-lRdrt]\n");
	  return (1);
	}
      process_allargs(ac, av, plist);
    }
  else
    process_all(ac, av);
  return (0);
}

int	main(int ac, char **av)
{
  my_ls(ac, av);
  return (0);
}
