/*
** my_swap.c for Day 04 in /home/maire_j/rendu/Piscine-C-Jour_04
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct  3 07:03:17 2013 Maire Jonathan
** Last update Tue Oct  8 09:18:51 2013 Maire Jonathan
*/

int	my_swap(int *a, int *b)
{
  int	a_tmp;
  int	b_tmp;
  
  a_tmp = *a;
  b_tmp = *b;
  *a = b_tmp;
  *b = a_tmp;

  return (0);
}
