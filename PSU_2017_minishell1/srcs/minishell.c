/*
** minishell.c for minishell1 in /home/maire_j/rendu/PSU_2017_minishell1
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Nov  4 10:54:02 2013 Maire Jonathan
** Last update Fri Nov 15 14:39:22 2013 
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "../libmy/my.h"
#include "../includes/hminishell.h"

char	*test_command(char *cmd, char **env)
{
  char	**paths;
  char	*res;
  int	i;
  int	success;

  i = 0;
  paths = get_path(env);
  while (paths[i])
    {
      res = my_catstr(paths[i], my_catstr("/", cmd));
      if ((success = access(res, F_OK)) == 0)
	return (res);
      free(res);
      i = i + 1;
    }
  return (0);
}

void	do_command2(char *cmd, char **env)
{
  char	**tabs;
  char	*res;
  pid_t	cpid;

  tabs = my_str_to_wordtab(cmd);
  if ((cpid = fork()) == 0)
    {
      if ((res = test_command(tabs[0], env)) == 0)
	{
	  my_putstr(tabs[0]);
	  my_putstr(" : command not found !\n");
	}
      else
	execve(res, tabs, env);
      free(res);
      free(tabs);
      exit(EXIT_SUCCESS);
    }
  wait(0);
}

void	do_command(char *cmd, char **env)
{
  char	**tabs;

  tabs = my_str_to_wordtab(cmd);
  if (my_strcmp(tabs[0], "getenv") == 0 && tabs[1] != 0)
    {
      if (getenv(tabs[1]) != 0)
	{
	  my_putstr(getenv(tabs[1]));
	  my_putchar('\n');
	}
      else
	my_putstr("This environment variable doesn't exist dude !\n");
    }
  else if (my_strcmp(tabs[0], "setenv") == 0 && tabs[1] != 0 && tabs[2] != 0)
    {
      if (tabs[3] != 0 || setenv(tabs[1], tabs[2], tabs[3]) == 0)
	my_putstr("Success !\n");
      else
	perror("setenv");
        my_putstr("\nUsage : setenv [NAME] [value] [overwrite 1/0]\n");
    }
  else if (my_strcmp(tabs[0], "unsetenv") == 0 && tabs[1] != 0)
    unset_env(tabs[1]);
  else
    do_command2(cmd, env);
}

int	minishell(char **env)
{
  char	*buff;
  int	buff_size;
  int	read_s;

  buff_size = 4096;
  if ((buff = malloc(4096 * sizeof(char))) == 0)
    return (1);
  while (buff != 0)
    {
      empty_buff(buff, read_s);
      my_putstr("\e[1;34m$> \e[0m");
      read_s = read(0, buff, buff_size);
      if (read_s > 1)
	{
	  if (my_strcmp(my_str_to_wordtab(buff)[0], "cd") == 0)
	    change_dir(buff, env);
	  else if (my_strcmp(my_str_to_wordtab(buff)[0], "exit") == 0)
	    exit(EXIT_SUCCESS);
	  else
	    do_command(buff, env);
	}
    }
  free(buff);
  return (0);
}

int	main(int ac, char **av, char **env)
{
  minishell(env);
  return (0);
}
