/*
** path.c for path.c in /home/maire_j/rendu/PSU_2017_minishell1
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Nov  8 12:41:32 2013 Maire Jonathan
** Last update Sun Nov 10 21:24:20 2013 Maire Jonathan
*/

#include <stdlib.h>
#include "../includes/hminishell.h"
#include "../libmy/my.h"

int	path_tab(char **env)
{
  int	i;

  i = 0;
  while (env[i])
    {
      if (my_strncmp(env[i], "PATH=", 5) == 0)
	return (i);
      i = i + 1;
    }
  return (i);
}

char	*user_tab(char *path, char **env)
{
  int	i;
  char	*user;

  i = 0;
  while (my_strncmp(env[i], my_catstr(path, "="), my_strlen(path) + 1) != 0)
    {
      i = i + 1;
    }
  user = my_strdup(env[i]);
  return (user);
}

char	**get_path(char **env)
{
  int	i;
  int	envp;

  i = 0;
  envp = path_tab(env);
  while (env[envp][i])
    {
      if (i < 5)
	env[envp][i] = ' ';
      if (env[envp][i] == ':')
	env[envp][i] = ' ';
      i = i + 1;
    }
  return (my_str_to_wordtab(env[envp]));
}
