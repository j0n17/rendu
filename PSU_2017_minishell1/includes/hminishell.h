/*
** minishell.h for minishell1 in /home/maire_j/rendu/PSU_2017_minishell1
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Nov  4 10:53:55 2013 Maire Jonathan
** Last update Sun Nov 10 22:11:22 2013 Maire Jonathan
*/

#ifndef HMINISHELL_H_
# define HMINISHELL_H_

void	empty_buff(char *, int);
char	*my_catstr(char *, char *);
int	path_tab(char **);
char	*user_tab(char *, char **);
char	**get_path(char **);
char	*my_rabb(char *, int);
char	*test_command(char *, char **);
void	do_ls(char *, char **);
char	*get_env(char *, char **);
void	change_dir(char *, char **);
void	unset_env(char *);

#endif /* HMINISHELL_H_ */
