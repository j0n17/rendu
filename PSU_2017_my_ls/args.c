/*
** args.c for args in /home/maire_j/rendu/PSU_2013_my_ls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov  3 16:00:52 2013 Maire Jonathan
** Last update Sun Nov  3 23:21:48 2013 Maire Jonathan
*/

#include <dirent.h>
#include <stdlib.h>
#include "libmy/my.h"

void	array_func(char *file, char *args)
{
  int	i;

  i = 0;
  while (args[i])
    {
      if (args[i] == 'R')
	rec_print(file);
      else if (args[i] == 'l')
	{
	  print_dir_l(file);
	}
      i = i + 1;
    }
}

void	proc_args_files(char **flist, char *args, int size)
{
  int	i;

  i = 0;
  while (i < size)
    {
      array_func(flist[i], args);
      i = i + 1;
    }
}

void	proc_args(char *args)
{
  int	i;

  i = 0;
  while (args[i])
    {
      if (args[i] == 'R')
	{
	  rec_print(".");
	}
      else if (args[i] == 'l')
	{
	  print_dir_l(".");
	}
      i = i + 1;
    }
}

int	rec_print(char *dir)
{
  DIR	*dirp;
  struct dirent	*d;
  char	*pathdir;

  dirp = opendir(dir);
  if (dirp == 0)
    {
      perror(dir);
      return (1);
    }
  while ((d = readdir(dirp)))
    {
      if (my_strncmp(d->d_name, ".", 1) == -1)
	{
	  my_putstr(d->d_name);
	  my_putstr("\n");
	  if (d->d_type == DT_DIR)
	    {
	      rec_print(d->d_name);
	    }
	}
    }
  closedir(dirp);
  return (0);
}

int	rec_print_l(char *dir)
{
  DIR	*dirp;
  struct dirent	*d;

  dirp = opendir(dir);
  if (dirp == 0)
    {
      perror(dir);
      return (1);
    }
  while ((d = readdir(dirp)))
    {
      if (my_strncmp(d->d_name, ".", 1) == -1)
	{
	  my_putstr(d->d_name);
	  my_putstr("\n");
	  if (d->d_type == DT_DIR)
	    {
	      rec_print_l(d->d_name);
	    }
	}
    }
  closedir(dirp);
  return (0);
}
