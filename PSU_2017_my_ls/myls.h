/*
** my_ls.h for  in /home/maire_j/rendu/myls-2013-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 28 11:50:32 2013 Maire Jonathan
** Last update Sun Nov  3 20:41:45 2013 Maire Jonathan
*/

#ifndef MYLS_H_
# define MYLS_H_

int	my_ls(int, char **);
char	*get_params(int, char **, char *);
int	check_p(char *, char *);
int	count_param(int, char **);
void	process_all(int, char **);
void	process_allargs(int, char **, char *);
void	proc_args_files(char **, char *);
void	proc_args(char *);
int	print_dir(char *);
int	count_flist(int, char **);
int	rec_print_l(char *);
int	rec_print(char *);
void	array_func(char *, char *);

#endif /* MYLS_H_ */
