/*
** long.c for long.c in /home/maire_j/rendu/PSU_2013_my_ls
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov  3 20:40:29 2013 Maire Jonathan
** Last update Sun Nov  3 23:35:43 2013 Maire Jonathan
*/

#include <dirent.h>

void	print_stat(char *file)
{
  my_get_guid(file, 0);
  my_putchar(' ');
  my_get_guid(file, 1);
  my_putchar(' ');
  my_get_mtime(file);
  my_putchar(' ');
}

int     print_dir_l(char *dir)
{
  DIR   *dirp;
  struct dirent *d;

  dirp = opendir(dir);
  if (dirp == 0)
    {
      my_putstr("my_ls: ");
      perror(dir);
      return (1);
    }
  while ((d = readdir(dirp)) != 0)
    {
      if (my_strncmp(d->d_name, ".", 1) != 0)
        {
	  print_stat(d->d_name);
          my_putstr(d->d_name);
          my_putchar('\n');
        }
    }
  closedir(dirp);
  return (0);
}
