/*
** colle.c for colle in /home/maire_j/rendu/Piscine-C-colles-Semaine_01
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct  5 12:11:27 2013 Maire Jonathan
** Last update Sun Oct  6 17:37:16 2013 Maire Jonathan
*/

int     print_x(int width, int height, int y)
{
  int   x;
  
  x = 1;
  while (x <= width)
    {
      fill(width, height, x, y);
      x = x + 1;
    }
}

int	print_y(int width, int height)
{
  int	y;
  
  y = 1;
  while (y <= height)
    {
      print_x(width, height, y);
      my_putchar('\n');
      y = y + 1;
    }
}

int	write_error()
{
  my_putchar('E');
  my_putchar('r');
  my_putchar('r');
  my_putchar('e');
  my_putchar('u');
  my_putchar('r');
  my_putchar('\n');
}

int	fill(int width, int height, int x, int y)
{
  if (width == 1 || height == 1)
    {
      my_putchar('*');
    }
  else if ((y == 1 && x == 1) || (y == height && x == width))
    {
      my_putchar('/');
    }
  else if ((y == height &&  x == 1) || (y == 1 && x == width))
    {
      my_putchar('\\');
    }
  else if ((x > 1 && x < width) && (y > 1 && y < height))
    {
      my_putchar(' ');
    }
  else 
    {
      my_putchar('*');
    }
}

int     colle(int width, int height)
{
  if (width < 0 || height < 0)
    {
      write_error();
    }
  else
    {
      print_y(width, height);
    }
}
