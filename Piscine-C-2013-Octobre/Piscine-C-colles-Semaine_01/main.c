/*
** main.c for main in /home/maire_j/rendu/Piscine-C-colles-Semaine_01
** 
** Made by Maire Jonathan & Soufflet Maxime
** Login   <maire_j@epitech.net> & <souffl_a@epitech.net>
** 
** Started on  Sat Oct  5 12:02:45 2013 Maire Jonathan
** Last update Sat Oct 26 20:19:24 2013 Maire Jonathan
*/

int	main(int argc, char **argv)
{
  if (argc == 3)
    {
      colle(my_getnbr(argv[1]), my_getnbr(argv[2]));
    }
  else
    {
      colle(5, 4);
    }
  return (0);
}

int	my_putchar(char c)
{
  write(1, &c, 1);
}
