/*
** display_str.c for display_str.c in /home/maire_j/rendu/Piscine-C-colles-Semaine_02
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct 12 15:05:40 2013 Maire Jonathan
** Last update Sun Oct 13 09:21:59 2013 Maire Jonathan
*/
#include <stdlib.h>

void	display_clear(char *str)
{
  int   i;

  i = 0;
  while (i < (my_strlen(str) + 1))
    {
      my_putchar(' ');
      i = i + 1;
    }
  my_putchar('\r');
  i = my_strlen(str);
}

char	*whitespace_array(char *str, char *dest)
{
  int	i;

  dest = malloc(my_strlen(str) + 1);
  i = 0;
  while (i < my_strlen(str))
    {
      dest[i] = ' ';
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}

void	display_from_l(char *str, int sleep_time)
{
  int	i;
  int	i1;
  int	i2;
  int	counter;
  char	*dest;

  i2 = 0;
  dest = whitespace_array(str, dest);
  i = my_strlen(str);
  while (i >= 0)
    {
      i1 = my_strlen(str) - i2;
      counter = 0;
      while (dest[i1])
	{
	  dest[i1] = str[counter];
	  my_putstrdisplay(dest);
	  counter = counter + 1;
	  i1 = i1 + 1;
	}
      usleep(sleep_time);
      i2 = i2 + 1;
      i = i - 1;
    }
  free(dest);
}

void	display_from_r(char *str, int sleep_time)
{
  int   i;
  int   i1;
  int   i2;
  int   counter;
  char  *dest;

  i2 = my_strlen(str);
  dest = whitespace_array(str, dest);
  i = 0;
  while (i <= my_strlen(str))
    {
      counter = 0;
      i1 = my_strlen(str) - i;
      while (str[i1])
        {
          dest[counter] = str[i1];
          my_putstrdisplay(dest);
          counter = counter + 1;
          i1 = i1 + 1;
        }
      usleep(sleep_time);
      i = i + 1;
    }
  free(dest);
}
