/*
** header.h for header in /home/maire_j/rendu/Piscine-C-Jour_12/ex_01
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 24 14:28:32 2013 Maire Jonathan
** Last update Thu Oct 24 15:17:42 2013 Maire Jonathan
*/
#ifndef HEADER_H_
# define HEADER_H_

struct list_param
{
  int	arc;
  char	**arv;
};

typedef struct list_param t_list;

t_list	*my_params_in_list(int ac, char **av);
int	view_struct(struct list_param *arg_list);
int	free_arg(t_list *arg_list);

#endif
