/*
** cat.c for cat in /home/maire_j/rendu/Piscine-C-Jour_13
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct 23 11:43:00 2013 Maire Jonathan
** Last update Thu Oct 24 22:42:54 2013 Maire Jonathan
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "../../Piscine-C-lib/my/my.h"

int	main(int argc, char **argv)
{
  int   fd;
  int   i;
  int	i1;
  char  buffer[1];

  i = 1;
  i1 = 0;
  if (argc > 1)
    {
      while (i < argc)
        {
          fd = open(argv[i], O_RDONLY);
          if (fd == 3)
            {
	      while (buffer[0] != 3 || i1 < sizeof(fd))
		{
		  read(fd, buffer, 1);
		  my_putchar(buffer[0]);
		  i1 = i1 + 1;
		}
              close(fd);
            }
          else
            {
              my_putstr("cat: ");
              my_putstr(argv[i]);
              my_putstr(": No such file or directory");
              my_putchar('\n');
            }
          i = i + 1;
        }
    }
  else
    {
      my_putstr("No file specified\n");
    }
  return (0);
}
