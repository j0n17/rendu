/*
** btree_create_node.h for btree_create_node in /home/maire_j/rendu/Piscine-C-Jour_14/ex_00
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 16:17:49 2013 Maire Jonathan
** Last update Fri Oct 25 16:52:02 2013 Maire Jonathan
*/

#ifndef BTREE_H_
# define BTREE_H_

typedef struct s_btree
 {
  struct s_btree *left;
  struct s_btree *right;
  void *item;
} t_btree;

t_btree	*bree_create_node(void *item);

#endif
