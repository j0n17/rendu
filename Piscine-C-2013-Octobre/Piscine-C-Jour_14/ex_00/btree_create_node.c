/*
** btree_create_node.c for btree_create_node in /home/maire_j/rendu/Piscine-C-Jour_14/ex_00
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 16:57:20 2013 Maire Jonathan
** Last update Fri Oct 25 17:36:59 2013 Maire Jonathan
*/
#include <stdlib.h>
#include "../../Piscine-C-include/my.h"
#include "btree.h"

t_btree	*btree_create_node(char *item)
{
  t_btree *elem;

  elem = malloc(sizeof(*elem));
  if (elem == NULL)
    {
      return (1);
    }
  elem->item = item;
  elem->left = *list;
  elem->right = *list;
  return (0);
}

void	my_show_list(t_btree *list)
{
  t_btree *tmp;
  
  tmp = list;
  while (tmp != NULL)
    {
      my_putstr(tmp->item);
      my_putchar('\n');
      tmp = tmp->right;
    }
}
