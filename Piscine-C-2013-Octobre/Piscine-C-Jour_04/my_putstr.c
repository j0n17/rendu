/*
** my_putstr.c for Day 04 in /home/maire_j/rendu/Piscine-C-Jour_04
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct  3 07:03:52 2013 Maire Jonathan
** Last update Thu Oct  3 12:04:14 2013 Maire Jonathan
*/

int	my_putstr(char *str)
{
  char	*next;
  
  next = str;
  while (*next != '\0')
    {
      my_putchar(*next);
      next = next + 1;
    }
}
