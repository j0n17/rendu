/*
** my_strlen.c for Day 04 in /home/maire_j/rendu/Piscine-C-Jour_04
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct  3 07:04:10 2013 Maire Jonathan
** Last update Thu Oct  3 14:48:22 2013 Maire Jonathan
*/

int	my_strlen(char *str)
{
  char	*next;
  int	i;
  
  i  = 0;
  next = str;
  while (*next != '\0')
    {
      next = next + 1;
      i = i + 1;
    }
  return (i);
}
