/*
** my_evil_str.c for Day 04 in /home/maire_j/rendu/Piscine-C-Jour_04
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct  3 07:04:35 2013 Maire Jonathan
** Last update Tue Oct  8 09:08:29 2013 Maire Jonathan
*/

int	count(char *str)
{
  char  *next;
  int   i;

  i = 0;
  next = str;
  while (*next != '\0')
    {
      next = next + 1;
      i = i + 1;
    }
  return (i);
}

int	print_char_array(char *str, int length, int i3)
{
  while (i3 <= length)
    {
      my_putchar(str[i3]);
      i3 = i3 + 1;
    }
}


char	*my_evil_str(char *str)
{ 
  int	length;
  int	i2;
  int	i3;
  int	rez;
  char	a_tmp;
  char	calc;
  
  i2 = 0;
  i3 = 0;
  length = count(str);
  rez = length / 2;
  while (i2 <= rez)
    {
      calc = length - i2;
      a_tmp = str[i2];
      str[i2] = str[calc];
      str[calc] = a_tmp;
      i2 = i2 + 1;
    }
  print_char_array(str, length, i3);
}
