/*
** my_str_to_wordtab.c for my_str_to_wordtab.c in /home/maire_j/rendu/Piscine-C-Jour_08/ex_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Oct 22 16:51:31 2013 Maire Jonathan
** Last update Tue Oct 22 18:47:49 2013 Maire Jonathan
*/

int	my_alphanum(char c)
{
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= 48 && c <= 57))
    {
      return (1);
    }
  else
    {
      return (0);
    }
}

char	*count_words(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (my_alphanum(str[i]) == 0)
	{
	  str[i] == ' ';
	  my_putchar('i');
	}
      i = i + 1;
    }
  my_putstr(str);
  my_putchar('\n');
  return (str);
}

int	**my_str_to_wordtab(char *str)
{
  count_words(str);
  return (0);
}
