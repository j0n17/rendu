/*
** my_strdup.c for my_strdup.c in /home/maire_j/rendu/Piscine-C-Jour_08
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  9 09:20:35 2013 Maire Jonathan
** Last update Thu Oct 10 10:18:51 2013 Maire Jonathan
*/
#include <stdlib.h>

char	*my_strdup(char *src)
{
  char	*dest;
  int	i;

  i = 0;
  dest = malloc(my_strlen(src) + 1);
  if (dest != '\0')
    {
      while (src[i])
	{
	  dest[i] = src[i];
	  i = i + 1;
	}
      dest[i] = '\0';
    }
  else
    {
      dest = '\0';
    }
  return (dest);
}
