/*
** do-op.h for do-op in /home/maire_j/rendu/Piscine-C-eval_expr
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 10:55:33 2013 Maire Jonathan
** Last update Sun Oct 27 18:37:25 2013 Maire Jonathan
*/

#ifndef DOOP_H_
# define DOOP_H_

int	addnb(int, int);
int	subnb(int, int);
int	modnb(int, int);
int	divnb(int, int);
int	multnb(int, int);
int	count_char(char *, char);
int	do_op(int, int, char);
int	eval_expr(char *);
char	*rem_spaces(char *);

#endif
