/*
** eval_expr.c for eval_expr in /home/maire_j/rendu/Piscine-C-eval_expr
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 09:05:08 2013 Maire Jonathan
** Last update Sun Oct 27 18:41:44 2013 Maire Jonathan
*/
#include <stdlib.h>
#include "../Piscine-C-include/my.h"
#include "doop.h"

int	count_char(char *str, char c)
{
  int	res;
  int	i;

  res = 0;
  i = 0;
  while (str[i])
    {
      if (str[i] == c)
	{
	  res = res + 1;
	}
      i = i + 1;
    }
  return (res);
}

int	do_op(int nb, int nb2, char op)
{
  int	i;
  char	*operator;
  int	(**operation)(int nb, int nb2);

  i = 0;
  operation = malloc(5);
  operator = malloc(6);
  operator = my_strdup("*/%+-");
  operation[0] = &multnb;
  operation[1] = &divnb;
  operation[2] = &modnb;
  operation[3] = &addnb;
  operation[4] = &subnb;
  while (operator[i])
    {
      if (op == operator[i])
	{
	  i = operation[i](nb, nb2);
	  return (i);
	}
      i = i + 1;
    }
  i = 0;
  free(operator);
  return (i);
}

char	*rem_spaces(char *str)
{
  char *good_str;
  int	size;
  int	i;
  int	j;

  size = my_strlen(str) - count_char(str, ' ') + 1;
  good_str = malloc(size);
  i = 0;
  j = 0;
  if (good_str != 0)
    {
      while (i < size - 1)
	{
	  while (str[j] == ' ' && str[j])
	    {
	      j = j + 1;
	    }
	  good_str[i] = str[j];
	  j = j + 1;
	  i = i + 1;
	}
      return (good_str);
    }
  return (0);
}

void	linear_op(int i, char *str)
{
  while (str[i])
    {
      
    }
}

int	eval_expr(char *str)
{
  char *pr_str;

  pr_str = rem_spaces(str);
  my_putstr(pr_str);
  do_op(20, 60, '-');
  return (0);
}
