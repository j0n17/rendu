/*
** main.c for Main in /home/maire_j/rendu/Piscine-C-eval_expr
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 09:02:41 2013 Maire Jonathan
** Last update Sun Oct 27 18:03:13 2013 Maire Jonathan
*/
#include "../Piscine-C-include/my.h"

int	main(int argc, char **argv)
{
  if (argc == 2)
    {
      eval_expr(argv[1]);
      my_putchar('\n');
    }
  else
    {
      my_putstr("Aucun argument de rentré\n");
    }
  return (0);
}
