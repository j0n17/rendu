/*
** do-op.c for do-op in /home/maire_j/rendu/Piscine-C-eval_expr
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 25 10:23:44 2013 Maire Jonathan
** Last update Fri Oct 25 14:21:11 2013 Maire Jonathan
*/

int	addnb(int fst_nb, int sec_nb)
{
  int	res;

  res = fst_nb + sec_nb;
  return (res);
}

int	multnb(int fst_nb, int sec_nb)
{
  int	res;

  res = fst_nb * sec_nb;
  return (res);
}

int	subnb(int fst_nb, int sec_nb)
{
  int	res;

  res = fst_nb - sec_nb;
  return (res);
}

int	modnb(int fst_nb, int sec_nb)
{
  int	res;

  if (sec_nb == 0)
    {
      my_putstr("Error, modulo'd by 0");
      return (0);
    }
  res = fst_nb % sec_nb;
  return (res);
}

int	divnb(int fst_nb, int sec_nb)
{
  int	res;
  if (sec_nb ==0)
    {
      my_putstr("Error, divided by 0");
      return (0);
    }
  res = fst_nb / sec_nb;
  return (res);
}
