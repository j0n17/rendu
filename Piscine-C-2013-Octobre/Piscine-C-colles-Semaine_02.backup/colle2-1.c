/*
** colle2-1.c for colle2-1.c in /home/maire_j/rendu/Piscine-C-colles-Semaine_02
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct 12 13:45:31 2013 Maire Jonathan
** Last update Sun Oct 13 00:47:47 2013 Maire Jonathan
*/

int	my_getnbr(char *str)
{
  int   nb;
  int   nb_r;
  int   a;

  a = 1;
  nb_r = 0;
  nb = 0;
  while (*str > '9' || *str < '0')
    {
      if (*str == '-')
        {
          a = -a;
        }
      str = str + 1;
    }

  while (*str <= '9' && *str >= '0')
    {
      nb_r = nb_r * 10;
      nb = *str - '0';
      str = str + 1;
      nb_r = nb_r + nb;
    }
  nb_r = nb_r * a;
  return (nb_r);
}

int	check_first_arg(char *str)
{
  int	i;
  int	counter;

  i = 0;
  counter = 0;
  while (str[i])
    {
      if (str[0] == '-' || i > 0 && str[i] >= '0' && str[i] <= '9')
	{
	  counter = counter + 1;
	}
      i = i + 1;
    }
  if (counter == my_strlen(str))
    {
      counter = 1;
    }
  else
    {
      counter = 0;
    }
  return (counter);
}

void	display_reverse(int speed, int argc, char **argv, int first_arg)
{
  int   i;

  i = first_arg + 1;
  while (i <= argc + 1)
    {
      if (i % 2 > 0)
	{
	  display_from_r(argv[i], speed);
	  display_clear(argv[i]);
	}
      else
	{
	  display_from_l(argv[i], speed);
	  display_clear(argv[i]);
	}
      i = i + 1;
    }
}

void	display(int speed, int argc, char **argv, int first_arg)
{
  int	i;
  
  i = first_arg + 1;
  while (i <= argc + 1)
    {
      if (i % 2 > 0)
	{
	  display_from_l(argv[i], speed);
	  display_clear(argv[i]);
	}
      else
	{
	  display_from_r(argv[i], speed);
	  display_clear(argv[i]);
	}
      i = i + 1;
    }
}

int	main(int argc, char **argv)
{
  if (argc == 1 || argc == 2 && check_first_arg(argv[1]) == 1)
    {
      my_putstr("./colle2-1: [-speed] word1 [word2 [word3...]]\n");
    }
  else 
    {
      if (check_first_arg(argv[1]) == 1)
	{
	  while (1)
	    {
	      is_pair(argc - 2, -my_getnbr(argv[1]), argv, 1);
	    }
	  }
      else
	{
	  while (1)
	    {
	      is_pair(argc - 1, 150000, argv, 0);
	    }
	}
    }
  return (0);
}
