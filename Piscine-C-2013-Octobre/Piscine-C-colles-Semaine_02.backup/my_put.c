/*
** my_put.c for my_put in /home/maire_j/rendu/Piscine-C-colles-Semaine_02
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct 12 13:46:50 2013 Maire Jonathan
** Last update Sun Oct 13 00:51:34 2013 Maire Jonathan
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i = i + 1;
    }
}

void	my_putstrdisplay(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      my_putchar(str[i]);
      i = i + 1;
    }
  my_putchar('\r');
}

int	my_strlen(char *str)
{
  int	i;
  
  i = 0;
  while (str[i])
    {
      i = i + 1;
    }
  return (i);
}

void    is_pair(int argc, int speed, char **argv, int first_arg)
{
  if (first_arg == 0)
    {
      if (argc % 2 == 0)
	{
	  display_reverse(speed, argc - 1, argv, first_arg);
	}
      else
	{
	  display_reverse(speed, argc - 1, argv, first_arg);
	  display(speed, argc - 1, argv, first_arg);
	}
    }
  else
    {
      if (argc % 2 == 0)
	{
	  display(speed, argc, argv, first_arg);
	}
      else
	{
	  display(speed, argc, argv, first_arg);
	  display_reverse(speed, argc, argv, first_arg);
	}
    }
}
