/*
** my_aff_comb.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 09:02:49 2013 Maire Jonathan
** Last update Wed Oct  2 15:35:22 2013 Maire Jonathan
*/

int	check_if_inf(char a, char b, char c)
{
  if ((a < b) && (b < c))
    {
      my_putchar(a);
      my_putchar(b);
      my_putchar(c);
      if ((a == 55) && (b == 56) && (c == 57))
	{
	}
      else
	{ 
	  my_putchar(44);
	  my_putchar(32);
        }
    }
}

int	my_aff_comb()
{
  char	a;
  char	b;
  char	c;

  a = 48;
  b = 48;
  c = 48;
  while (a <= 57)
    {
      while (b <= 57)
	{
	  while (c <= 57)
	    {
	      check_if_inf(a, b, c);
	      c = c + 1;
	    }
	  c = 48;
	  b = b + 1;
	}
      b = 48;
      a = a + 1;
    }
}
