/*
** my_aff_comb2.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 12:32:43 2013 Maire Jonathan
** Last update Wed Oct  2 20:49:43 2013 Maire Jonathan
*/

int	check_if(int z, int y, char a, char b, char c, char d)
{
  
  if (z < y)
    {
      my_putchar(a);
      my_putchar(b);
      my_putchar(32);
      my_putchar(c);
      my_putchar(d);
      my_putchar(44);
      my_putchar(32);
    }
}

int	check_if_equal(char a, char b, char c, char d)
{
  while (c <= 57)
    {
      while (d <= 57)
	{
	  int	i;
	  int	i1;
	  int	i2;
	  int	i3;
	  int	z;
	  int	y;

	  i = a - 48;
	  i1 = b - 48;
	  i2 = c - 48;
	  i3 = d - 48;
	  z = i * 10 + i1;
	  y = i2 * 10 + i3;
	  check_if(z, y, a, b, c, d);
	  d = d + 1;
	}
      d = 48;
      c = c + 1;
    }
}

int	my_aff_comb2()
{
  char	a;
  char	b;
  char	c;
  char	d;

  a = 48;
  b = 48;
  c = 48;
  d = 48;
  while (a <= 57)
    {
      while (b <= 57)
	{
	  check_if_equal(a, b, c, d);
	  c = 48;
	  b = b + 1;
	}
      b = 48;
      a = a + 1;
    }
}
