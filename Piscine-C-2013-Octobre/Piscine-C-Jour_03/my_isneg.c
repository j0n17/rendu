/*
** my_isneg.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 07:56:13 2013 Maire Jonathan
** Last update Wed Oct  2 10:04:38 2013 Maire Jonathan
*/

int	my_isneg(int n)
{
  if (n < 0)
    {
      my_putchar('N');
    }
  else if (n >= 0)
    {
      my_putchar('P');
    }
  else
    {
      my_putchar('E');
    }
}
