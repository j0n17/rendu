/*
** my_aff_chiffre.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 07:49:17 2013 Maire Jonathan
** Last update Wed Oct  2 10:08:35 2013 Maire Jonathan
*/

int	my_aff_chiffre()
{
  int	i;

  i = 48;
  while (i <= 57)
    {
      my_putchar(i);
      i = i + 1;
    }
}
