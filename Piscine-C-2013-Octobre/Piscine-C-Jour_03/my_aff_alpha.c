/*
** my_aff_alpha.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 06:55:14 2013 Maire Jonathan
** Last update Wed Oct  2 16:01:43 2013 Maire Jonathan
*/ 

int	my_aff_alpha()
{
  char	c;

  c = 97;
  while (c < 123)
    {
      my_putchar(c);
      c = c + 1;
    }
}
