/*
** my_aff_revalpha.c for Day 03 in /home/maire_j/rendu/Piscine-C-Jour_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  2 07:42:40 2013 Maire Jonathan
** Last update Wed Oct  2 10:03:41 2013 Maire Jonathan
*/

int	my_aff_revalpha()
{
  char	c;
  
  c = 122;
  while (c > 96)
    {
      my_putchar(c);
      c = c - 1;
    }
}
