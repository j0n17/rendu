/*
** sapin.c for sapin in /home/maire_j/rendu/Piscine-C-sapin
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct  3 06:44:44 2013 Maire Jonathan
** Last update Mon Oct  7 07:13:57 2013 Maire Jonathan
*/

void	print_stars(int nb_stars, int max_stars)
{
  int	i;
  
  i = 0;
  if (nb_stars < max_stars)
    {
      while (i < nb_stars)
	{
	  my_putchar('*');
	  i = i + 1;
	}
      nb_stars = nb_stars + 2;
      print_stars(nb_stars, max_stars);
    }
}

void	print_lines(int ct_rec, int max_stars)
{
  if (ct_rec <= max_lines(ct_rec))
    {
      print_stars(ct_rec, max_stars);
      ct_rec = ct_rec + 1;
      print_lines(ct_rec, max_stars);
    }
}

void	rec_size(int size, int ct_rec)
{
  int	min_stars;
  int	max_stars;

  if (size > 1)
    {
      if ((size * 7) % 2 > 0)
	{
	  min_stars = size * 7 - 2;
	}
      else
	{
	  min_stars = size * 7 - 3;
	}
    }
  else
    {
      min_stars = 1;
    }
  if (ct_rec < size)
    {
      print_lines(0, max_stars);
      ct_rec = ct_rec + 1;
      rec_size(size, ct_rec);
    }
}

int	max_lines(int size)
{
  int	mod;

  mod = (size * 7) % 2;
  if (mod > 0)
    {
      size = size * 7;
    }
  else 
    {
      size = size * 7 - 1;
    }
  return (size);
}

void    sapin(int size)
{
  rec_size(size, 0);
}
