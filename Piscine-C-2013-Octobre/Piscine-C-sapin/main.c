/*
** main.c for main in /home/maire_j/rendu/Piscine-C-sapin
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct  5 19:33:00 2013 Maire Jonathan
** Last update Mon Oct  7 06:14:13 2013 Maire Jonathan
*/

int	my_putchar(char c)
{
  write(1, &c, 1);
}

int	main()
{
  sapin(2);
}
