/*
** main.c for minilibX in /home/maire_j/rendu/Piscine-C-X
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 24 10:07:21 2013 Maire Jonathan
** Last update Thu Oct 24 18:49:30 2013 Maire Jonathan
*/
#include <mlx.h>

int	draw_cube(void *mlx_ptr, void *win_ptr, int size, int pos_h, int pos_v)
{
  int	i_h;
  int	i_v;
  int	color;

  i_h = 0;
  i_v = 0;
  color = rand();
  while (i_v < size)
    {
      while (i_h < size)
	{
	  mlx_pixel_put(mlx_ptr, win_ptr, pos_h + i_h, pos_v + i_v, color);
	  i_h = i_h + 1;
	}
      i_h = 0;
      i_v = i_v + 1;
    }
}

int	draw_los(void *mlx_ptr, void *win_ptr, int size, int pos_h, int pos_v)
{
  int	i;

  i = 0;
  while (i < size * 50)
    {
      mlx_pixel_put(mlx_ptr, win_ptr, rand()%1000, rand()%1000, 0x00FFFFFFF);
      i = i + 1;
    }
}

int	draw_triangle_left(void *mlx_ptr, void *win_ptr, int size, int pos_h, int pos_v)
{
  int	i;
  int	i1;
  int	color;


  i = 0;
  i1 = 0;
  color = rand();
  while (i < size)
    {
      while (i1 < i)
	{
	  mlx_pixel_put(mlx_ptr, win_ptr, pos_h - i, pos_v + i1 + i, color);
	  mlx_pixel_put(mlx_ptr, win_ptr, pos_h + i, pos_v + i1, color);
	  i1 = i1 + 1;
	}
      i1 = 0;
      i = i + 1;
    }
}

int	draw_triangle_right(void *mlx_ptr, void *win_ptr, int size, int pos_h, int pos_v)
{
  int i;
  int i1;
  int color;


  i = 0;
  i1 = 0;
  color = rand()%800*10;
  while(i < size)
    {
      while (i1 < i)
	{
          mlx_pixel_put(mlx_ptr, win_ptr, pos_h - i, pos_v + i1, color);
          i1 = i1 + 1;
        }
      i1 = 0;
      i = i + 1;
    }
}

int	loop_display(void *mlx_ptr, void *win_ptr)
{
  int	i;
  int	rand2;
  int	rand1;

  i = 0;
  while (1)
    {
      while (i < 5000)
	{ 
	  rand2 = rand()%1000;
	  rand1 = rand()%1000;
          draw_cube(mlx_ptr, win_ptr, rand()%100, rand()%1000, rand()%1000);
	  draw_cube(mlx_ptr, win_ptr, rand()%100, rand()%1000, rand()%1000);
	  draw_triangle_right(mlx_ptr, win_ptr, rand()%100, rand()%1000, rand()%1000);
	  
	  if (i > 5)
	    draw_triangle_left(mlx_ptr, win_ptr, rand()%100, rand()%1000, rand()%1000);

	  /*draw_los(mlx_ptr, win_ptr, rand()%100, rand()%1000, rand()%1000);
	  /*mlx_string_put(mlx_ptr, win_ptr, rand1, rand2, 0x00FFFFFF, "1");
	    mlx_string_put(mlx_ptr, win_ptr, rand2, rand1, 0x00FFFFFF, "0");*/
	  usleep(300000);
	  /*mlx_clear_window(mlx_ptr, win_ptr);*/
	  i = i + 1;
	}
      i = 5;
    }
}

int	my_fst_win()
{
  void	*mlx_ptr;
  void	*win_ptr;
  int	x;
  int	y;

  x = 1000;
  y = 1000;
  mlx_ptr = mlx_init();
  win_ptr = mlx_new_window(mlx_ptr, x, y, "Tolo");
  loop_display(mlx_ptr, win_ptr);
}

int     main()
{
  my_fst_win();
  return (0);
}
