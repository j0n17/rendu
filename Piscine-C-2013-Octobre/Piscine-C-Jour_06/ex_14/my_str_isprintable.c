/*
** my_str_isprintable.c for my_str_isprintable in /home/maire_j/rendu/Piscine-C-Jour_06/ex_14
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 17:42:15 2013 Maire Jonathan
** Last update Tue Oct  8 09:45:09 2013 Maire Jonathan
*/

int	my_str_isprintable(char *str)
{
  int   i;
  int   var_returned;

  var_returned = 0;
  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] >= 33 && str[i] <= 126))
        {
	  i = i + 1;
	  var_returned = 1;
        }
      else
        {
	  var_returned = 0;
        }
      return (var_returned);
    }
  return (var_returned);
}
