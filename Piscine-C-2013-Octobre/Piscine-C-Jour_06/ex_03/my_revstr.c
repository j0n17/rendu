/*
** my_revstr.c for my_revstr.c in /home/maire_j/rendu/Piscine-C-Jour_06/ex_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 09:31:03 2013 Maire Jonathan
** Last update Wed Oct  9 08:42:20 2013 Maire Jonathan
*/

char	*my_revstr(char *str)
{
  char	c_tmp;
  int	i;
  int	i1;

  i = 0;
  i1 = my_strlenngth(str) - 1;
  while (i < i1)
    {
      c_tmp = str[i];
      str[i] = str[i1];
      str[i1] = c_tmp;
      i = i + 1;
      i1 = i1 - 1;
    }
  return (str);
}

int	my_strlength(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
