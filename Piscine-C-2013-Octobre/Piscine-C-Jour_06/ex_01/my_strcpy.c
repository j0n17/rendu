/*
** my_strcpy.c for my_strcpy in /home/maire_j/rendu/Piscine-C-Jour_06/ex_01
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 07:16:28 2013 Maire Jonathan
** Last update Thu Oct 10 08:27:34 2013 Maire Jonathan
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
    dest[i] = '\0';
  if (i != 0)
    {
      dest[i] = '\0';
    }
  return (dest);
}
