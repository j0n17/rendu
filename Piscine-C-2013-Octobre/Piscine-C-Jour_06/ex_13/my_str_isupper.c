/*
** my_str_isupper.c for my_str_isupper in /home/maire_j/rendu/Piscine-C-Jour_06/ex_13
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 17:42:15 2013 Maire Jonathan
** Last update Tue Oct  8 09:44:52 2013 Maire Jonathan
*/

int	my_str_isupper(char *str)
{
  int   i;
  int   var_returned;

  var_returned = 1;
  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] >= 65 && str[i] <= 90))
        {
	  i = i + 1;
	  var_returned = 1;
        }
      else
        {
          if (str[i] == '\0')
            {
              i = i + 1;
              var_returned = 1;
              return (var_returned);
            }
          else
            {
              var_returned = 0;
            }
          return (var_returned);
        }
    }
  return (var_returned);
}
