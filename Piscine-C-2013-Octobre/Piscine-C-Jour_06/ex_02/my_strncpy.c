/*
** my_strncpy.c for my_strncpy in /home/maire_j/rendu/Piscine-C-Jour_06/ex_02
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 07:16:28 2013 Maire Jonathan
** Last update Mon Oct  7 20:06:56 2013 Maire Jonathan
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;
  
  i = 0;
  while (src[i] != '\0')
    {
      if (i < n)
	{
	  dest[i] = src[i];
	}
      else
	{
	  dest[i] = '\0';
	}
      i = i + 1;
    }
  return (dest);
}
