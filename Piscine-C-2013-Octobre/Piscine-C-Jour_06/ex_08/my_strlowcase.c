/*
** my_strlowcase.c for my_strlowcase.c in /home/maire_j/rendu/Piscine-C-Jour_06/ex_08
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct  7 16:52:00 2013 Maire Jonathan
** Last update Wed Oct  9 08:43:15 2013 Maire Jonathan
*/

char    *rec_capital(char *str, int size, int ct_rec)
{
  if (str[ct_rec] != '\0')
    {
      if (str[ct_rec] >= 65 && str[ct_rec] <= 90)
        {
          str[ct_rec] = str[ct_rec] + 32;
          ct_rec = ct_rec + 1;
        }
      else if (str[ct_rec] < 65 || str[ct_rec] > 90)
        {
          ct_rec = ct_rec + 1;
        }
      rec_capital(str, size, ct_rec);
    }
  return (str);
}

char    *my_strlowcase(char *str)
{
  str = rec_capital(str, my_strlength(str), 0);
  return (str);
}

int     my_strlength(char *str)
{
  int   i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
