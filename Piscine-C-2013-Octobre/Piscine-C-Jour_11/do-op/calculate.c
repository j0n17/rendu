/*
** calculate.c for calculate in /home/maire_j/rendu/Piscine-C-Jour_11/do-op
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 21 16:53:26 2013 Maire Jonathan
** Last update Tue Oct 22 09:48:06 2013 Maire Jonathan
*/

void     divide(char *first, char *second, char *third)
{
  int   result;

  result = 0;
  if (second[0] == '/')
    {
      if (my_getnbr(third) > 0)
	{
	  result = my_getnbr(first) / my_getnbr(third);
	  my_put_nbr(result);
	}
      else
	{
	  my_putstr("Stop : divide by zero");
	}
    }
}

void    modulo(char *first, char *second, char *third)
{
  int   result;

  result = 0;
  if (second[0] == '%')
    {
      if (my_getnbr(third) > 0)
	{
	  result = my_getnbr(first) % my_getnbr(third);
	  my_put_nbr(result);
	}
      else
	{
	  my_putstr("Stop : modulo by zero");
	}
    }
}

void    multiply(char *first, char *second, char *third)
{
  int   result;

  result = 0;
  if (second[0] == '*')
    {
      result = my_getnbr(first) * my_getnbr(third);
      my_put_nbr(result);
    }
}

void	add(char *first, char *second, char *third)
{
  int   result;

  result = 0;
  if (second[0] == '+')
    {
      result = my_getnbr(first) + my_getnbr(third);
      my_put_nbr(result);
    }
}

void    substract(char *first, char *op, char *third)
{
  int   result;

  result = 0;
  if (op[0] == '-')
    {
      result = my_getnbr(first) - my_getnbr(third);
      my_put_nbr(result);
    }
  else
    {
      if (op[0] != 43 && op[0] != 45 && op[0] != 47)
	{
	  if (op[0] != 42 && op[0] != 37)
	    {
	      my_put_nbr(0);
	    }
	}
    }
}
