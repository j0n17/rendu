/*
** do-op.c for do-op in /home/maire_j/rendu/Piscine-C-Jour_11/do-op
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 21 13:19:10 2013 Maire Jonathan
** Last update Tue Oct 22 09:30:55 2013 Maire Jonathan
*/
int     divide(char *, char *, char *);
int     modulo(char *, char *, char *);
int     substract(char *, char *, char *);
int     add(char *, char *, char *);
int     multiply(char *, char *, char *);

int	main(int argc, char **argv)
{
  int	(*test[5])(char *, char *, char *);
  int	i;

  i = 0;
  test[0] = &divide;
  test[1] = &multiply;
  test[2] = &add;
  test[3] = &modulo;
  test[4] = &substract;
  if (argc == 4)
    {
      while (i < 5)
	{
	  test[i](argv[1], argv[2], argv[3]);
	  i = i + 1;
	}
    }
}
