/*
** my_strcat.c for ex_01 in /home/maire_j/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  9 18:09:33 2013 Maire Jonathan
** Last update Fri Oct 11 16:00:31 2013 Maire Jonathan
*/

int	length_string(char *str)
{
  int	i;
  
  i = 0;
  while (str[i])
    {
      i = i + 1;
    }
  return (i);
}

void	putputchar(char c)
{
  write(1, &c, 1);
}

void	putputstr(char *str)
{
  int	i;
  
  i = 0;
  while (str[i])
    {
      putputchar(str[i]);
      i = i + 1;
    }
}

char    *my_strcat(char *dest, char *src)
{
  int	i;
  int	i1;

  i1 = 0;
  i = length_string(dest);
  while (src[i1])
    {
      dest[i] = src[i1];
      i = i + 1;
      i1 = i1 + 1;
    }
  dest[i] = '\0';
  return (dest);
}
