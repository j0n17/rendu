/*
** my_rev_params.c for my_rev_params in /home/maire_j/rendu/Piscine-C-Jour_07/ex_03/my_rev_params
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Oct  9 12:11:58 2013 Maire Jonathan
** Last update Wed Oct  9 13:37:59 2013 Maire Jonathan
*/

void    my_writechar(char c)
{
  write(1, &c, 1);
}

void    my_writestring(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      my_writechar(str[i]);
      i = i + 1;
    }
}

int     main(int argc, char **argv)
{
  int	i;

  i = 1;
  while (i <= argc)
    {
      my_writestring(argv[argc - i]);
      my_writechar('\n');
      i = i + 1;
    }
  return (0);
}
