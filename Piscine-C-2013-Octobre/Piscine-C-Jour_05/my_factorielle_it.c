/*
** my_factorielle_it.c for Day 05 in /home/maire_j/rendu/Piscine-C-Jour_05
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct  4 07:46:00 2013 Maire Jonathan
** Last update Fri Oct  4 09:46:39 2013 Maire Jonathan
*/

int	is_int(int i)
{
  if ((i > 0) && (i <= 2147483647))                                           
    {
      return (i);                                                         
    }                                                                          
  else                                                                         
    {                                                                          
      return (0);                                                              
    }
}

int	my_factorielle_it(int nb)
{
  int	result;
  int	counter;

  result = nb;
  counter = nb - 1;
  if (nb <= 0)
    {
      return (0);
    }
  else
    {
      while (counter >= 1)
	{
	  result = result * counter;
	  counter = counter - 1; 
	}
    }
  return (is_int(result));
}
