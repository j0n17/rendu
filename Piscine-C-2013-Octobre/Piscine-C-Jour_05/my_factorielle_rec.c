/*
** my_factorielle_rec.c for Day 05 in /home/maire_j/rendu/Piscine-C-Jour_05
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct  4 08:28:05 2013 Maire Jonathan
** Last update Fri Oct  4 10:11:05 2013 Maire Jonathan
*/

int	rec_condition(int nb, int counter)
{
  if (counter > 0)
    {
      nb = nb * counter;
      counter = counter - 1;
      rec_condition(nb, counter);
    }
  else if ((nb >= 0))
    {
      return (nb);
    }
  else
    {
      return (0);
    }
}

int	my_factorielle_rec(int nb)
{
  int	counter;
  int	resultat;

  counter = nb - 1;
  return (rec_condition(nb, counter));
}
