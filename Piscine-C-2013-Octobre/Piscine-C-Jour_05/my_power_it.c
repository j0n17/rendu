/*
** my_power_it.c for Day 05 in /home/maire_j/rendu/Piscine-C-Jour_05
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct  4 10:29:26 2013 Maire Jonathan
** Last update Fri Oct  4 13:28:53 2013 Maire Jonathan
*/

int	my_power_it(int nb, int power)
{
  int	tmp_nb;
  int	i;

  i = 1;
  tmp_nb = nb;
  if (power > 0)
    {
      while (i < power)
	{
	  tmp_nb = tmp_nb * nb;
	  i = i + 1;
	}
      return (tmp_nb);
    }
  else if ((power = 0) || ((nb == 0) && (power ==0)))
    {
      return (1);
    }
  else
    {
      return (0);
    }
}
