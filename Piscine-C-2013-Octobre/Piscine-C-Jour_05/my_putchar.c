/*
** my_putchar.c for putchar in /home/maire_j/rendu/Piscine-C-Jour_05
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct  4 07:47:06 2013 Maire Jonathan
** Last update Fri Oct  4 07:47:30 2013 Maire Jonathan
*/

int	my_putchar(char c)
{
  write(1, &c, 1);
}
