/*
** func.c for func in /home/maire_j/rendu/Piscine-C-colles-Semaine_03.backup
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Oct 27 01:56:18 2013 Maire Jonathan
** Last update Sun Oct 27 10:31:35 2013 Maire Jonathan
*/

#include "glue.h"
#include "../Piscine-C-include/my.h"

void	colle1(int *size, int *size2, char *buff)
{
  if (buff[0] == 'o')
    {
      my_putstr("[colle1-1] ");
    }
}

void	colle2(int *size, int *size2, char *buff)
{
  if (buff[0] == '/' || buff [0] == '*')
    {
      my_putstr("[colle1-2] ");
    }
}

void	colle3(int *size, int *size2, char *buff)
{
  int	end;
  int	end_fst;

  end = my_strlen(buff) - 2;
  end_fst = *size / *size2 - 1;
  if (buff[0] == 'A' && buff[end_fst] == 'A' && buff[end] == 'C')
    {
      my_putstr("[colle1-3] ");
    }
}

void	colle4(int *size, int *size2, char *buff)
{  
  int	end;
  int	end_fst;

  end = my_strlen(buff) - 2;
  end_fst = *size / *size2 - 1;
  if (buff[0] == 'A' && buff[end] == 'C' && buff[end_fst] == 'C')
    {
      my_putstr("[colle1-4] ");
    }
}

void	colle5(int *size, int *size2, char *buff)
{
  int	end;

  end = my_strlen(buff) - 2;
  if (buff[0] == 'A' && buff[end] == 'A')
    {
      my_putstr("[colle1-5] ");
    }
}
