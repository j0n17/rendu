/*
** glue.c for glue in /home/maire_j/rendu/Piscine-C-colles-Semaine_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct 26 23:38:40 2013 Maire Jonathan
** Last update Sun Oct 27 10:51:47 2013 Maire Jonathan
*/

#include "../Piscine-C-include/my.h"
#include "glue.h"

void	print_size(int *size, int *size2)
{
  my_putchar('[');
  my_put_nbr(*size / *size2);
  my_putchar(']');
  my_putchar(' ');
  my_putchar('[');
  my_put_nbr(*size2);
  my_putchar(']');
}

int	count_col(char *buff, int *size)
{
  int	i;

  i = 0;
  while (buff[i] && buff[i] != '\n')
    {
      if (buff[i] != '\n')
        {
          *size = *size + 1;
        }
      i = i + 1;
    }
  return (*size);
}

int	count_lines(char *buff, int *size2)
{
  int	i;
  
  i = 0;
  while (buff[i])
    {
      if (buff[i] == '\n')
        {
          *size2 = *size2 + 1;
        }
      i = i + 1;
    }
  return (*size2);
}

void	count_stuff(char *buff, int *size, int *size2)
{
  count_lines(buff, size2);
  count_col(buff, size);
}
