/*
** declarestuff.c for declare stuff in /home/maire_j/rendu/Piscine-C-colles-Semaine_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Oct 27 10:48:58 2013 Maire Jonathan
** Last update Sun Oct 27 11:34:14 2013 Maire Jonathan
*/

#include <stdlib.h>

char **declare_stuff()
{
  char **buffers;
  char *buff_tmp;
  char *buff;

  buffers = malloc(2);
  buff_tmp = malloc(80000);
  buff = malloc(2);
  buffers[0] = buff;
  buffers[1] = buff_tmp;
  return (buffers);
}

void	enumerate(int *size, int *size2)
{
  my_putstr("[colle1-3] ");
  print_size(size, size2);
  shapes(size, size2);
  my_putstr("|| [colle1-4] ");
  print_size(size, size2);
  shapes(size, size2);
  my_putstr("|| [colle1-5] ");
  print_size(size, size2);
  shapes(size, size2);
}
