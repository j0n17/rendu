/*
** glue.h for glue.h in /home/maire_j/rendu/Piscine-C-colles-Semaine_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Sat Oct 26 23:39:28 2013 Maire Jonathan
** Last update Sun Oct 27 11:34:42 2013 Maire Jonathan
*/

#ifndef GLUE_H_
# define GLUE_H_

void	print_size(int *, int *);
void	count_stuff(char *, int *, int *);
int	count_col(char *, int *);
int	count_lines(char *, int *);
void	colle1(int *, int *, char *);
void	colle2(int *, int *, char *);
void	colle3(int *, int *, char *);
void	colle4(int *, int *, char *);
void	colle5(int *, int *, char *);
int	check_glue(int *, int *, char *, char *);
char	**declare_stuff();
void	enumerate(int *, int *);

#endif /* GLUE_H_ */
