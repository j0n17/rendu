/*
** match.c for match.c in /home/maire_j/rendu/Piscine-C-match-nmatch
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 10:09:25 2013 Maire Jonathan
** Last update Sun Oct 13 23:41:38 2013 Maire Jonathan
*/

#include "my.h"

int	check_str(char *str, char *pat)
{
  int	i;
  int	i1;
  int	counter;

  i = 0;
  i1 = 0;
  counter = 0;
  while (str[i])
    {
      while (pat[i1] == '*')
	{
	  i1 = i1 + 1;
	}
      if (pat[i1] == str[i])
	{
	  i = i + 1;
	  i1 = i1 + 1;
	  counter = counter + 1;
	}
      else if (pat[i1 - 1] == str[i - 1] && pat[i1] != str[i])
	  return (0);
      else
	  i = i + 1;
    }
  return (counter);
}

int	match(char *s1, char *s2)
{
  int	counter;

  counter = 0;
  if (my_strlen(s2) > my_strlen(s1))
    {
      if (s2[my_strlen(s2) - 1] == '*' && check_str(s1, s2) > 1)
	{
	  return (1);
	}
      else
	{
	  return (0);
	}
    }
  else
    {
      if (check_str(s1, s2) > 0)
	{
	  return (1);
	}
      else
	{
	  return (0);
	}
    }
}
