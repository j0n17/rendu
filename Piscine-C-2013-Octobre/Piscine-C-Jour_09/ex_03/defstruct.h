/*
** defstruct.h for defstruct in /home/maire_j/rendu/Piscine-C-Jour_09/ex_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Oct 22 16:17:06 2013 Maire Jonathan
** Last update Thu Oct 24 14:10:33 2013 Maire Jonathan
*/

#ifndef STRUCT_H_
# define STRUCT_H_

struct	s_stock_par
{
  char	*first_name;
  char	*last_name;
  int	age;
};

typedef struct s_stock_par t_person;

t_person *new_person(char *first_name, char *last_name, int age);

char	view_struct(struct s_stock_par *person);
char	free_person(t_person *person);

#endif
