/*
** my_param_to_tab.c for Param to tab in /home/maire_j/rendu/Piscine-C-Jour_09/ex_03
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Oct 22 11:24:29 2013 Maire Jonathan
** Last update Thu Oct 24 14:20:16 2013 Maire Jonathan
*/
#include "defstruct.h"
#include <stdlib.h>
#include <string.h>
#include "../../Piscine-C-include/my.h"

char	*dupdup(char *str)
{
  int	i;

  i = 0;
  
}

char	view_struct(struct s_stock_par *person)
{
  my_putstr(person->first_name);
  my_putchar('\n');
  my_putstr(person->last_name);
  my_putchar('\n');
  my_put_nbr(person->age);
  my_putchar('\n');
  return (0);
}

char	free_person(t_person *person)
{
  if (person == NULL)
    {
      return (1);
    }
  else if (person->first_name != NULL && person->last_name != NULL)
    {
      free(person->first_name);
      free(person->last_name);
      free(person);
      return (0);
    }
}

t_person *new_person(char *first_name, char *last_name, int age)
{
  t_person *person;
  
  person = malloc(sizeof(t_person));
  if (person == NULL)
    {
      return (NULL);
    }
  person->first_name = my_strdup(first_name);
  person->last_name = my_strdup(last_name);
  person->age = age;
  return (person);
}

int	main(int argc, char **argv)
{
  t_person	*jon;
  t_person	*roger;
  
  jon = new_person("Jonathan", "M.....", 64);
  roger = new_person("Tolooooo", "azeaze", 20);
  view_struct(jon);
  view_struct(roger);
  return (0);
}
