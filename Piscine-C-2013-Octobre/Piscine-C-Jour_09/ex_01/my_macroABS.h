/*
** my_macroABS.h for my_macroABS.h in /home/maire_j/rendu/Piscine-C-Jour_09/ex_01
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Oct 10 08:45:32 2013 Maire Jonathan
** Last update Fri Oct 11 18:52:28 2013 Maire Jonathan
*/

#ifndef MY_MACROABS_H_
# define MY_MACROABS_H_

# define ABS(value) ((value < 0) ? -value : value)

#endif
