/*
** main.c for ff in /home/maire_j/.Cfiles
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Nov  5 11:42:06 2013 Maire Jonathan
** Last update Mon Nov 11 10:53:31 2013 Maire Jonathan
*/

#include "my.h"
#include <fcntl.h>

void	check_e(int ac, char **av, char *buff)
{
  int	i;
  int	e;

  i = 1;
  e = 0;
  while (i < ac)
    {
      if (my_strcmp(av[i], "-e") == 0)
	e = 1;
      i = i + 1;
    }
  if (buff[0] == '\n' && e == 1)
    {
      my_putchar('$');
      my_putstr(buff);
    }
  else
    my_putstr(buff);
}

int	main(int ac, char **av)
{
  char	buff[2];
  int	i;
  int	fp;
  int	r_s;

  i = 1;
  fp = 0;
  r_s = 0;
  buff[1] = '\0';
  if (ac == 1)
    my_putstr("Usage : ./mycat [file] [-e]\n");
  while (i < ac)
    {
      if ((fp = open(av[i], O_RDONLY)) != -1)
	{
	  while ((r_s = read(fp, buff, 1)) != 0)
	    {
	      check_e(ac, av, buff);
	    }
	  i = i + 1;
	}
      else
	i = i + 1;
      close(fp);
    }
  return (0);
}
