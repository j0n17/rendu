/*
** funcs.h for funcs.h in /home/maire_j/rendu/PSU_2017_minitalk/includes
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Thu Nov 14 12:11:09 2013 
** Last update Thu Nov 14 12:43:19 2013 
*/

#ifndef FUNCS_H_
# define FUNCS_H_

void	empty_buff(char *, int);
int	binary_dec(char *);

#endif /* FUNCS_H_ */
