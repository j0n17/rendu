/*
** main.c for server-main in /home/maire_j/rendu/PSU_2017_minitalk/srcs/server
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Nov 13 13:55:11 2013 
** Last update Fri Nov 15 13:03:33 2013 
*/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include "../../includes/funcs.h"

char	g_binary[9];

void	add_binary(char c)
{
  int	i;

  i = 0;
  while (g_binary[i] != 'a' && i < 8)
    i = i + 1;
  g_binary[i] = c;
  if (i == 7)
    {
      my_putchar(binary_dec(g_binary));
      empty_buff(g_binary, 8);
    }
}

void	my_handler(int sig)
{
  if (sig == SIGUSR1)
    add_binary('0');
  else if (sig == SIGUSR2)
    add_binary('1');
  else
    exit(EXIT_SUCCESS);
}

int	my_powertwo(int power, int nb)
{
  int   tmp_nb;
  int   i;

  i = 1;
  tmp_nb = nb;
  if (power > 0)
    {
      while (i < power)
	{
	  tmp_nb = tmp_nb * nb;
	  i = i + 1;
	}
      return (tmp_nb);
    }
  else if ((power == 0) || ((nb == 0) && (power == 0)))
    return (1);
  else
    return (0);
}

int	binary_dec(char *str)
{
  int	i;
  int	j;
  int	nb;

  i = 0;
  j = 7;
  nb = 0;
  while (str[i] && j >= 0)
    {
      nb = nb + (str[i] - '0') * my_powertwo(j, 2);
      j = j - 1;
      i = i + 1;
    }
  return (nb);
}

int	main()
{
  pid_t pid;

  pid = getpid();
  empty_buff(g_binary, 8);
  my_putstr("Server's PID is : ");
  my_put_nbr(pid);
  my_putchar('\n');
  if (signal(SIGUSR1, my_handler) == SIG_ERR)
    return (0);
  if (signal(SIGUSR2, my_handler) == SIG_ERR)
    return (0);
  exit(EXIT_SUCCESS);
}
