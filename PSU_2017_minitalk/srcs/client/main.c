/*
** main.c for client-main in /home/maire_j/rendu/PSU_2017_minitalk/srcs/client
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Wed Nov 13 13:54:45 2013 
** Last update Fri Nov 15 12:19:57 2013 
*/

#include <signal.h>

int	send_binary(pid_t pid, char *c)
{
  int	i;

  i = 0;
  while (c[i])
    {
      if (c[i] == '0')
	{
	  if (kill(pid, SIGUSR1) != 0)
	    return (-1);
	  usleep(10);
	}
      else if (c[i] == '1')
	{
	  if (kill(pid, SIGUSR2) != 0)
	    return (-1);
	  usleep(10);
	}
      i = i + 1;
    }
  return (0);
}

int	convert_string(pid_t pid, char c)
{
  char	result[9];
  int	i;
  int	nb;

  i = 7;
  nb = c;
  while (i >= 0)
    {
      result[i] = c % 2 + 48;
      c = c / 2;
      i = i - 1;
    }
  result[8] = '\0';
  if (send_binary(pid, result) != 0)
    my_putstr("An error occured while sending the message ! Check the PID.\n");
  return (0);
}

int	minitalk_client(char *pid, char *str)
{
  pid_t pid_id;
  int	i;

  pid_id = my_getnbr(pid);
  i = 0;
  if (pid_id < 0)
    return (-1);
  while (str[i])
    {
      if (convert_string(pid_id, str[i]) != 0)
	return (1);
      i = i + 1;
    }
  return (0);
}

int	main(int ac, char **av)
{
  if (ac != 3)
    {
      my_putstr("Syntax : [ServerPID] [String]\n");
      return (-1);
    }
  if (minitalk_client(av[1], av[2]) != 0)
    my_putstr("Error, PID must exist and be a positive number !\n");
  return (0);
}
